// Funciones; bloques de código reutilizable
var hello = function () { // Se define la función var, tal como se hace con las variables
	console.log("Estoy diciendo hello"); // Código de la función
};
hello(); // Se hace el llamado a la función

// Emplear una funnción 2 veces
var fullName = ""; // Se declara la variable fullName
var name; // Se declara la variable name
var firstLetter; // Se declara la variable firstLetter
var fixName = function () { // Se define la función
	firstLetter = name.substring(0, 1);
	name = firstLetter.toUpperCase()+name.substring(1);
	fullName = fullName + " " + name;
};
name = prompt("Teclea tu primer nombre (todo en minúsculas):"); // Se requiere interacción de usuario para asignar un valor
fixName(); // Se llama a la función
name = prompt("Teclea tu apellido (todo en minúsculas):"); // Se requiere interacción del usuario para asignar un valor
fixName(); // Se llama nuevamente a la función
console.log("Tu nombre completo es " + fullName); // Se imprime el resultado

// Variable globales y locales
var greeting = "Ahoy"; // Esta es una variable global y puede usarse en cualquier función subsecuente
var greet = function () {
	var greeting = "Hello"; // Se define una variable con el mismo nombre de una variable global pero son distintas; esta es una variable local y sólo puede usarse dentro de la función; las variables locales no cambian a las globales
	console.log(greeting);
};
greet(); // Al llamar a la función se imprime el valor de la varibale local, no de la variable global

// Parámetros
var sayHelloTo = function (name) { // Al definir una función se pueden agregar parámetros entre () para recibir valores que serán tratados como variables locales
	console.log("Hello " + name);
};
sayHelloTo("Ivan"); // Al llamar a la función se envía un valor como parámetro entre ()

// Return
var number = function (x) {
	return x; // El método return regresa un valor después de ejecutar una sentencia
};
number(3); // Se inicializa la función y se envía un parámetro

// Regresar el cuadrado de un número
var square = function (x) {
	return x * x; // Esto regresa el valor del parámetro recibido multiplicado por sí mismo
};
square(3); // Se inicializa la función y se envía un parámetro

// Pares y nones
var isEven = function (n) { // Se define la función isEven para que reciba el parámetro n
	if ( n % 2 === 0 ) { // Si el remanente de de dividir n entre 2 es igual a cero...
		return true; // ... el número es par y se obtiene un true
	}
	else {
		return false; // ... en todo caso es non y se obtiene un false
	};
};
console.log(isEven(1)); // Se imprime en consola el resultado de la función isEven al enviar un parámetro
console.log(isEven(2));
console.log(isEven(999));

// Número perdido
var lost = ["4", "8", "15", "16", "23", "42"]; // Se crea un array con 6 índices
var count = lost.length; // Se declara una variable es determina el número de índices del array; en este caso el método length no obtiene la longitud de caracteres de un string sino la longitud de índices de un array
var isLost = function (n) { // Se define la función que recibe como parámetro el valor de n
	for ( i = 0; i < count; i++ ) { // La función se inicializa con i que tiene un valor igual a 0 al ser el índice inicial de cualquier array; el bucle se ejecutará tantas veces como índices posea el array
		if ( n === lost[i] ) { // Si el valor de n coincide con el valor de cualquiera de los índices del array...
			return true; // Entronces se obtiene un true
		};
	};
	return false; // ... en todo caso se obtiene un false; el false en este caso se coloca fuera del if y es el return predeterminado en caso de que el bucle for no regrese lo contrario
};
var n = prompt("Dame un número:"); // Se requiere interacción del usuario para asignar un valor a n
if ( isLost(n) ) { // Si la función isLost se ejecutó con éxito y por lo tanto se obtuvo un true...
	console.log(n + " es un número perdido"); // ... se obtiene la confirmación de que el valor se encuentra en alguno de los índices del array y que por lo tanto es un número perdido...
}
else {
	console.log(n + " no es número perdido") // ... de lo contrario se obtiene una confirmación de que no lo es
};

// La cuarta parte de un número
var quarter = function (n) { // Se define la función que recibe el valor del parámetro n
	return ( n / 4 ); // Se obtiene el resultado de dividir n entre 4, es decir, su cuarta parte
};
if ( quarter(4) === 1 ) { // Si la cuarta parte de 4 es igual a 1...
	console.log("La declaración es cierta"); // ... entonces la declaración es cierta...
}
else {
	console.log("La declaración es falsa"); // ... de otro modo la declaración es falsa
};

// Reutilizazión de funciones dentro de otras
var isOdd = function (x) { // Se define la función para determinar si un número es non o no
	if ( x % 2 !== 0 ) {
		return true; // Si lo es se obtiene un true
	}
	else {
		return false; // Si no lo es se obtiene un false
	};
};
var iEven = function (x) { // Se define la función para determinar si un número es para o no
	return !isOdd(x); // Se llama a la función isOdd pero se antepone un ! para indicar un "no es" o "no cumple con la función"
};

// Varios parámetros en una función
var area = function (w, l) { // Se pueden manejar varios parámetros en una función si se separan entre ,
	return ( w * l );
};