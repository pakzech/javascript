// FizzBuzz
var myNumber = prompt("Dame un número para hacer FizzBuzz:"); // Solicita un número para hacer el FizzBuzz
var i; // Se declara la variable i
for ( i = 1; i <= myNumber; i++ ) { // Se define un for para generar todos los números del 1 al que el usuario haya tecleado
	if ( (i % 3 === 0) && (i % 5 === 0) ) { // Si el número es divisible entre 3 y 5...
		console.log("FizzBuzz"); // ... entonces es un FizzBuzz
	}
	else if ( i % 3 === 0 ) { // Si es divisible entre 3...
		console.log("Fizz"); // ... entonces es un Fizz
	}
	else if ( i % 5 === 0 ) { // Si es divisible entre 5
		console.log("Buzz"); // ... entonces es un Buzz
	}
	else { // Si no es divisible entre 5 o 3
		console.log(i); // Sólo se obtiene el número
	}
};