// $ vs $()
var jQuery = $; // Cuando $ se usa solo, equivale a escribir jQuery, pudiendo utilizar los métodos nativos de jQuery
var jQueryObject = $(); // $() es una función que regresa un objeto de jQuery, pudiendo utilizar los métodos disponibles para todos los objetos de jQuery
for (var k in jQuery) { // Con este for in...
	alert(k); // ... se obtienen todos los métodos disponibles de jQuery
};
for (var k in jQueryObject) { // Con este for in...
	alert(k); // ... se obtienen todos los métodos disponibles para un objeto de jQuery
};

// Verificación de arrays
var safeLength = function (object) { // Esta función recibe un argumento...
	if ($.isArray(object) { // ... y determina con el método isArray() de jQuery si el argumento recibido es un array (se obtiene true o false) y en ese caso...
		return object.length; // ... regresa la longitud del array
	}
	else { // De otra manera...
		return 0; // ... regresa 0
	};
};

// Iterar en arrays y objetos de jQuery
var myArray = [1, 2, 3, 4]; // Se declara un array
$.each(myArray, function (index, value) { // Se usa el método de jQuery each() que recibe dos argumentos, el primero es un array o un objeto, el segundo es una función que a su vez toma dos argumentos, el índice y el valor...
	alert(value); // ... y ejecuta una o varias sentencias
});
$myElement.each(function () {}); // El método each puede llamarse directamente desde un objeto de jQuery, en ese caso sólo recibe como argumento una función

// Trabajo con DATA de HTML
var $myElement = $('#myId'); // Se crea un objeto de jQuery con el elemento con ID myId
var data = $myElement.data('x'); // En este caso el método data() actúa como getter al obtener el valor del atributo DATA con el índice recibido como argumento
$myElement.data('y', '5'); // En este caso el método data() actúa como setter al crear un nuevo atributo DATA con el índice correspondiente al primer argumento recibido y con valor equivalente al segundo argumento recibido
$myElement.removeData('y'); // El método removeData() elimina la información almacenada en el atributo DATA enviado como parámetro; este método sin embargo no remuve el atributo propiamente dicho

// Trabajo con atributos
var $allTheLinks = $('a'); // Este objeto contiene todos los links del documento
$allTheLinks.each(function () { // Mediante el método each() se itera en cada link...
	var actualLink = $(this).attr('href'); // ... se usa el método attr() como getter al recibir un solo argumento, en este caso el valor del atributo HREF para almacenarlo en una variale; notar que para poder trabajar correctamente con vada elemento a iterar, primero se convierte a un objeto de jQuery mediante $(this)...
	$(this).attr('href', '/life' + actualLink); // ... se usa el método attr() como setter al recibir dos argumentos, el primero corresponde al atributo y el segundo al nuevo valor a asignar
});
$mylink.removeAttr('href'); // El método removeAttr() elimina el atributo recibido como argumento del elemento al cual se aplica; en este caso se está eliminando el atributo HREF del elemento

// Creación de nuevos elementos
var $newDiv = '<div id="new-div"></div>'; // Se crea una variable con un string para formar un nuevo DIV
$newDiv.html('Soy un nuevo DIV'); // Se agrega nuevo contenido HTML al DIV
$('body').append($newDiv); // Mediante el método append() se inserta el nuevo DIV en el DOM
var $newHeading = $('<h2 />'); // Se crea un nuevo objeto de jQuery pasando como argumento una etiqueta cerrada por sí misma, este método es más efectivo que el anterior
$newHeading.attr('id', 'new-h2'); // Se agrega un ID al nuevo encabezado mediante el método attr()
$newHeading.html('Soy un nuevo encabezado'); // Se agrega nuevo contenido HTML al H2
$('body').append($newHeading); // Mediante el método append() se inserta el nuevo H2 en el DOM

// Creación de elementos con atributos en un solo paso
var $myNewLink = $('<a />', { // Se crea un nuevo objeto de jQuery enviando dos parámetros: un string de una etiqueta cerrada por sí misma y un objeto con una propiedad y valor por cada atributo a añadir
	href: '#home', // La propiedad href del objeto se convertirá en el atributo HREF
	id: 'homeLInk' // La propiedad id del objeto se convertirá en el atributo ID
});

// Inserción de elementos al principio y al final mediante una relación padre-hijo
var $divToAppend = $('<div />'); // Un objeto de jQuery creando un nuevo DIV
var $divToPrepend = $('<div />'); // Un objeto de jQuery creando un nuevo DIV
$divToAppend.html('Append'); // Se agrega contenido al DIV
$divToPrepend.html('Prepend'); // Se agrega contenido al DIV
$divToAppend.appendTo('#insertHere'); // Se aplica el método appendTo() al elemento y se envía como argumento un selector, de esta manera se inserta el elemento $divToAppend como último hijo del elemento con ID insertHere
$divToPrepend.prependTo('#insertHere'); // Se aplica el método prependTo() al elemento y se envía como argumento un selector, de esta manera se inserta el elemento $divToPrepend como primer hijo del elemento con ID insertHere
$('#insertHere').append($divToAppend); // Usando el método append() se puede insertar contenido como último hijo en ese elemento
$('#insertHere').prepend($divToPrepend); // Usando el método prepend() se puede insertar contenido como primer hijo en ese elemento
/*
Diferencias entre los métodos appendTo()/prependTo() y append()/prepend():
Los métodos appendTo() y prependTo() se aplican al elemento a insertar pasando como argumento el elemento en el que se insertará
Los métodos append() y prepend () se aplican al elemento en el que se insertará el contenido pasando como argumento el elemento a insertar
Los métodos appendTo() y prependTo() sólo pueden recibir como argumento elementos ya creados
Los métodos append() y prepend() pueden recibir adicionalmente strings con contenido HTML creando elementos de forma implícita
*/

// Inserción de elementos antes y después mediante una relación de hermanos
var $divToInsertBefore = $('<div />'); // Un objeto de jQuery creando un nuevo DIV
var $divToInsertAfter = $('<div />'); // Un objeto de jQuery creando un nuevo DIV
$divToInsertBefore.html('Before').addClass('before'); // Se agrega contenido y se asigna una clase al DIV
$divToInsertAfter.html('After').addClass('after'); // Se agrega contenido y se asigna una clase al DIV
$divToInsertBefore.insertBefore('#insertHere'); // Se aplica el método insertBefore() al elemento y se envía como argumento un selector, de esta manera se insertará el elemento $divToInsertBefore justo antes del elemento con ID insertHere
$divToInsertAfter.insertAfter('#insertHere'); // Se aplica el método insertAfter() al elemento y se envía como argumento un selector, de esta manera se insertará el elemento $divToInsertAfter justo después del elemento con ID insertHere
$('#insertHere').before($divToInsertBefore); // Usando el método before() se puede insertar contenido justo antes de ese elemento
$('#insertHere').after($divToInsertAfter); // Usando el método after() se puede insertar contenido justo después de ese elemento
/*
Diferencias entre los métodos insertBefore()/insertAfter() y before()/after():
Los métodos insertBefore() e insertAfter() se aplican al elemento a insertar pasando como argumento el elemento en el que se insertará
Los métodos before() y after() se aplican al elemento que se tomará como referncia para insertar el contenido pasando como argumento el elemento a insertar
Los métodos insertBefore() e insertAfter() sólo pueden recibir como argumento elementos ya creados
Los métodos before() y after() pueden recibir adicionalmente strings con contenido HTML creando elementos de forma implícita
*/

// Envolver elementos
$('.wrapMe').wrap($('<div />').addClass('wrapper')); // Se aplica el método wrap() a todos los elementos con clase wrapMe enviando como argumento un nuevo DIV con clase wrapper; de esta manera cada uno de los elementos será envuento por un DIV de con clase wrapper

// Mover elementos
$('#moveInside').prependTo('#reference'); // El elemento con ID moveInside se encuentra actualmente fuera del elemento con ID reference; aplicándole un método de inserción puede mover su posición en el DOM, en este caso colocarse como primer hijo del elemento con ID reference
$('#moveOutside').insertBefore('#reference'); // El elemento con ID moveOutside actualmente es hijo del elemento con ID reference; aplicándole un método de inserción puede mover su posición en el DOM, en este caso convertirse en hermano del elemento con ID reference

// Copiar elementos
var cloneAndAppend = function ($elementToClone, $elementToAppendItTo) { // Una función que recibe dos objetos de jQuery como argumento; el primero será el elemento a clonar, el segundo será en el cual se insertará la copia creada
	$elementToClone.clone(true).appendTo($elementToAppendItTo); // Se aplica el método clone() al primer objeto para crear una copia profunda del mismo con todos sus elementos hijos incluidos; posteriormente se inserta la copia creada en el segundo objeto
};
/*
Opciones del método clone():
Al recibir un true como argumento, crea una copia con todos los atributos, información (contenida en atributos DATA) y eventos adjuntos al elemento clonado
El valor predeterminado es false, con lo que no se copia la información ni los eventos del elemento
*/

// Vaciar elementos
$('#trash').html(''); // Al establecer el contenido HTML de un elemento como un string vacío, se vacía su contenido de una forma insegura que podría llevar al colapso del sitio
$('#trash').empty(); // Al aplicar el método empty() a un elemento, se vacía su contenido de forma segura, eliminando primero todo tipo de información y eventos adjuntos de la memoria

// Eliminar elementos
$('#removeMe').remove(); // Al aplicar el método remove() al elemento con ID removeMe, éste y todos sus elementos hijos se eliminan completamente del DOM
$('#myElement .removeMe').remove(); // En este caso se eliminarán todos los elementos con clase removeMe contenidos dentro del elemento con ID myElement
$('div').remove('.removeMe'); // El método remove() puede tomar un selector como argumento para eliminar elementos de forma selectiva; en este caso hay un objeto que contiene todos los DIVs del documento pero sólo elimina aquellos que tienen la clase removeMe
$('#removeMe').detach(); // El método detach() trabaja de forma similar al método remove(), sin embargo, en este caso toda la información y eventos adjuntos se mantendrán en la memoría para posteriormente poder insertar el elemento nuevamente