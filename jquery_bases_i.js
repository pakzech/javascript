// Función jQuery
var element = document.getElementById('myElement'); // Este es un selector de JavaScript tradicional, seleccionando el elemento con ID myElement del documento actual
$element = $(element); // La varibale $element es un objeto de jQuery basado en la variable element
/*
Los selectores de jQuery se usan mediante la función jQuery()
$() es un atajo de la función jQuery()
La función, sea que se use mediante jQuery() o mediante $() siempre regresa un objeto de jQuery
La variable $element no es diferente de otras variables de JavaScript por tener $ al principio, sólo es un convencionalismo que señala que se trata de un elemento de jQuery
*/

// Selección por etiqueta
$allOfTheDivs = $('div'); // La variable $allOfTheDivs es un objeto de jQuery que selecciona la etiqueta DIV, por lo que contiene todos los DIVs del documento
$allOfTheLinks = $('a'); // La variable $allOfTheLinks es un objeto de jQuery que selecciona la etiqueta A, por lo que contiene todos los links del documento

// Selección por ID
$element = $('#elementId'); // La variable $element es un objeto de jQuery que selecciona el elemento con ID elementId

// Selección por clase
$element = $('.myClass'); // La variable $element es un objeto de jQuery que selecciona la clase myClass, por lo que contiene todos los elementos del documento con dicha clase

// Otros selectores
$element = $('div .thingy'); // Selecciona los elementos de la clase thingy que son descendientes de un DIV
$element = $('.thingy > a'); // Selecciona los links que son hijos directos de un elemento con la clase thingy
$element = $('li:first-child'); // Selecciona los elementos de lista que son primer hijo de su elemento padre

// Añadir elementos
$h1elements = $('h1'); // El objeto h1elements contiene todos los H1 del documento
$h1andh2elements = $h1elements.add('h2'); // Este nuevo objeto contiene todos los H1 contenidos en el objeto h1elements más los elementos H2 usando el método add()

// Selección de primer y último elemento
$element = $('div').first(); // Este objeto selecciona todos los DIVs del documento para luego, con el método first(), únicamente el primer DIV en el DOM
$element = $('div').last(); // Este objeto selecciona todos los DIVs del documento para luego, con el método last(), únicamente el último DIV en el DOM

// Selección por índice
$element = $('div').eq(2); // Este objeto selecciona todos los DIVs del documento para luego, con el método eq(), tomar como argumento un entero y seleccionar el div correspondiente a dicho índice, en este caso se selecciona el tercer DIV ya que los índices comienzan en 0
$element = $('div').eq(-3); // Cuando el método eq() recibe un número negativo el índice comienza a contarse desde el último hacia atrás, en este caso selecciona el tercer DIV contando desde el último DIV hacia atrás

// Filtrado
$x = $element.filter('.myClass'); // Este objeto contiene todos los elementos con clase myClass que a su vez se encuentran dentro del objeto $element
$y = $element.filter('li'); // Este objeto contiene los elementos de lista que a su vez se encuentran dentro del objeto $element
/*
El método filter sirve para filtrar elementos se acuerdo al argumento recibido
El método filter puede tomar como argumento un selector, una función, un elemento o un objeto de jQuery
*/

// Exclusión
$allOfTheDivs = $('div'); // Este objeto selecciona todos los DIVs del documento
$thirdDiv = $allOfTheDivs.eq(2); // Este objeto selecciona sólo el tercer DIV usando el método eq()
$allButTheThird = $allOfTheDivs.not($thirdDiv); // Este objeto selecciona todos los DIVs tomando el objeto allOfTheDivs pero excluyendo el tercer DIV mediante el método not()

// Selección de hijos por recorrido del DOM
$children = $('#myId').children(); // Este objeto selecciona, mediante el método children(), todos los elementos hijos del elemento con ID myId; si fuera preciso, el método children() podría recibir un argumento, por ejemplo un selector se clase
var getChildren = function ($that) { // Esta función toma como argumento un objeto de jQuery...
	return $that.children(); // ... y regresa otro objeto con todos los elementos hijos de dicho objeto
};

// Selección de descendientes por recorrido del DOM
$spanDescendants = $('#divOne').find('span'); // Este objeto selecciona, mediante el método find(), todos los SPANs que se encuentran dentro del elemento con ID divOne
/*
Diferencias entre el método children() y el método find():
1. El método children() trabaja sólo en el nivel de hijos directos; el método find() trabaja en cualquier nivel de profundidad sobre todos los descendientes
2. A diferencia del método children(), el método find() requiere un argumento obligatorio, en todo caso puede usarse el operador de estrella: $element.find('*')
*/

// Selección de padres por recorrido del DOM
$parent = $('#myId').parent(); // Este objeto selecciona, mediante el método parent(), el padre inmediato del elemento con ID myId; si el objeto contiene varios elementos, el método selecciona el padre inmediato de todos los elementos en el objeto
var getParent = function ($that) { // Esta función toma como argumento un objeto de jQuery...
	return $that.parent(); // ... y regresa otro objeto con el padre inmediato del elemento o elementos contenidos en dicho objeto
};
$parents = $('#myId').parents(); // Este objeto selecciona, mediante el método parents(), todos los ascendientes del elemento con ID myId; si el objeto contiene varios elementos, el método selecciona los ascendientes de todos los elementos en el objeto
var getParents = function ($that) { // Esta función toma como argumento un objeto de jQuery...
	return $that.parents(); // ... y regresa otro objeto con los ascendientes del elemento o elementos contenidos en dicho objeto
};
$closest = $('#myId').closest($('h1')); // Este objeto selecciona, mediante el método closest(), el elemento H2 más cercano hacia arriba en el árbol del DOM al elemento con ID myId
/* Diferencias entre los métodos parent(), parents() y closest():
1. El método parent() trabaja sólo en el nivel de padre directo, el método parents() trabaja en cualquier nivel superior sobre todos los ascendientes
2. El método parents() puede tomar un argmento para filtrar los ascendientes que coincidan con dicho argumento
3. El método closest() requiere un argumento obligatorio
4. El método parents() barre el árbol del DOM hacia arriba sobre todos los ascendientes del o los elementos en el objeto y luego, si se usa un argumento, efectúa el filtrado; el método closest barre el árbol del DOM hacia arriba sólo hasta encontrar el elemento que coincida con el argumento recibido
*/

// Selección de hermanos
$siblings = $('#myId').siblings(); // Este objeto selecciona, mediante el método siblings(), todos los hermanos (al mismo nivel) del elemento con ID myId; si fuera preciso, el método children() podría recibir un argumento selector para filtrar elementos
var getSiblings = function ($that) { // Esta función toma como argumento un objeto de jQuery...
	return $that.siblings(); // ... y regresa otro objeto con los hermanos del elemento o elementos contenidos en dicho objeto
};

// Selección de pares y nones
$evenDivs = $('div:even'); // El selector :even aplicado a cualquier otro selector, obtiene los elementos pares que coincidan
$oddDivs = $('div:odd'); // El selector :odd aplicado a cualquier otro selector, obtiene los elementos nones que coincidan

// HTML dinámico
$newElement = $myElement.html(); // En este caso, el método html(), al estar sin argumentos, actúa como un getter, obteniendo el contenido HTML del elemento
$myElement.html('<strong>Hi</strong>'); // En este otro caso, el método html() recibe un string en HTML como argumento por lo que actúa como setter, asignando nuevo contenido HTML al elemento
var fillHTML = function ($sink, $source) { // Esta función recibe dos objetos de jQuery como argumento...
	var source = $source.html(); // ... primero obtiene el contenido HTML del objeto $source y lo almacena en una nueva variable...
	$sink.html(source); // ... para luego, usando el mismo método html() para agregar el contenido HTML obtenido al objeto $sink
};

// Trabajar con medidas
var width = $myElement.width(); // En este caso, el método width() actúa como getter
$myElement.height(50); // En este caso, el método height() actúa como setter
var rotate = function ($that) { // Esta función toma un objeto de jQuery como argumento...
	var height = $that.height(); // ... usa el método height() como getter para almacenar su altura actual en una variable...
	var width = $that.width(); // ... usa el método width() como getter para almacenar su anchura actual en una variable...
	$that.height(width); // ... usa el método height() como setter para actulizar la altura del elemento...
	$that.width(height); // ... usa el método width() como setter para actualizar la anchura del elemento
};
/*
Ambos métodos, height() y width(), al actuar como setters reciben como argumento un entero que se convertirá en una medida en píxeles
Si se requiere, puede cambiarse la unidad de medida enviando un string como argumento
*/

// Trabajar con estilos de CSS
var color = $myElement.css('background-color'); // En este caso, el método css() actúa como getter al tomar un solo argumento, regresando el valor establecido mediante CSS para el elemento
$myElement.css('color', '#000'); // En este caso, el método css() actúa como setter al recibir dos argumentos, el primero corresponde a una propiedad y el segundo al valor a asignar

// Trabajar con clases de CSS
$myElement.addClass('myClass'); // El método addClass() agrega la clase recibida como argumento
$myElement.removeClass('myClass'); // El método removeClass() quita la clase recibida como argumento
$myElement.removeClass(); // Si no recibe argumentos, el método removeClass() quita todas las clases de un elemento
$myElement.hasClass('newClass'); // El método hasClass() determina si un elemento posee o no una clase y regresa true o false
$myElement.toggleClass('anotherClass'); // El método toggleClass() agrega una clase si no está presente o la elimina si lo está