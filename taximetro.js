// Calcula el costo de un viaje en taxi
var taxiFare = function (milesTravelled, hourOfDay) { // Se define la función que recibe como parámetros el número de millas recorridas y la hora del día
	var baseFare = 2.50; // Se declara el banderazo de salida
	var costPerMile = 2.00; // Se declara el costo por milla
	var nightSurcharge = 0.50; // Se declara un costo extra como tarifa nocturna
	var cost = baseFare + (milesTravelled * costPerMile); // Se declara el costo como resultado de multiplicar el número de millas recorridas por el costo por milla sumado al banderazo
	if ( hourOfDay >= 20 || hourOfDay < 6 ) { // El costo extra por tarifa nocturna aplica después de las 8pm y hasta las 6am
		cost += nightSurcharge; // En ese caso el costo es igual a sumarle la tarifa nocturna
	};
	return cost; // La función regresa el costo final
}