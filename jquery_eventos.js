// Ready
$('document').ready(function () { // El evento ready(), aplicado al documento, se usa para tener disponible todo el código dentro de la función anónima una vez terminada la carga de la página
	// Código a ejecutar
});

// Click
$myLink.click(function () { // El evento click() se captura aplicándolo a un elemento, pudiendo realizar cualquier función
	// Código a ejecutar
});

// On
$('document').on('ready', function () { // El manejador de eventos on() es universal y debe recibir dos argumentos, el primero el evento, el segundo el código a ejecutar
	// Código a ejecutar
});
$myLink.on('click', function () { // En este caso, el manejador on() captura el evento click y una función anónima a realizar una vez que el evento se lleve a cabo
	// Código a ejecutar
});

// Manejo de múltiples eventos con On
$('#sampleID').on({ // on() puede manejar más de un evento; en tal caso recibe un solo argumento: un objeto... 
	'mousedown': function (e) { // ... cada evento se maneja como un método del objeto; el argumento e sólo captura el lugar donde se efectúa el evento, para este ejemplo puede no ser necesario; en este caso se captura el click del mouse cuando el botón baja...
		$(this).html('down'); //... y el método cambia el contenido HTML del elemento
	},
	'mouseup': function (e) { // ... un segundo método en el que se captura el click del mouse cuando el botón sube...
		$(this).html('up'); //... y el método cambia el contenido HTML del elemento
	}
});

// Trabajo con nodos objetivos de un evento
$('div').click(function (e) { // Se captura el evento click() sobre todos los DIVs del documento y se comienza una función anónima que recibe el argumento e, mismo que a su vez almacena el lugar donde se efectuó dicho evento...
	var $target = $(e.target); // ... posteriormente, mediante la propiedad target aplicada a e (el evento), se almacena en un objeto de jQuery el elemento que accionó el evento: el nodo objetivo...
	alert($target.id); // ... se envía una alerta que indica el ID del nodo objetivo
	if ($target.hasClass('blue')) { // El definir el nodo objetivo permite trabajar con ese elemento de cualquier forma posible, por ejemplo, en este caso se determina si el nodo objetivo posee la clase blue...
		$target.removeClass('blue').addClass('green'); // ... y en ese caso se elimina la clase y se agrega una clase nueva
	}
});

// Movimientos del mouse
$('document').mousemove(function (e) { // El evento mousemove() captura el movimiento del mouse; acciona una función anónima que recibe como argumento el evento...
	$('#mouseFollow').offset({ // ... mediante el método offset(), que envía un objeto como argumento...
		left: e.pageX + 10, // ... se usa la propiedad left para establecer un nuevo posicionamiento (propiedad left de CSS)...
		top: e.pageY +10 // ... se usa la propiedad top para establecer un nuevo posicionamiento (propiedad top de CSS)...
	});
});
$('div').hover(function () { // El método hover() captura el evento de posicionar el mouse sobre un elemento
	$(this).addClass('hover'); // Código a ejecutar
});
$('div').mouseenter(function () { // El método mouseenter() funciona de forma similar al método hover() pero captura el solo evento de entrar con el mouse sobre un elemento, no se posicionarlo
	$(this).addClass('enter'); // Código a ejecutar
});
$('div').mouseleave(function () { // El método mouseleave() captura cuando el mouse sale fuera del elemento
	$(this).removeClass('enter'); // Código a ejecutar
});

// Posicionamiento de elementos
var offset = $myElement.offset(); // El método offset() actúa en este caso como getter, obteniendo un objeto con las propiedades left y top, correspondientes al posicionamiento en píxeles de un elemento respecto al documento
alert(offset.left); // Se obtiene el valor del posicionamiento en el eje X del elemento respecto al documento
alert(offset.top); // Se obtiene el valor del posicionamiento en el eje Y del elemento respecto al documento
$myElement.offset({ // El método offset() actúa en este caso como setter, recibiendo como argumento un objeto
	left: e.pageX + 10, // Se usa la propiedad left para establecer un nuevo posicionamiento (propiedad left de CSS)
	top: e.pageY +10 // Se usa la propiedad top para establecer un nuevo posicionamiento (propiedad top de CSS)
});
var position = $myElement.position(); // El método position() actúa se la misma forma, como getter y setter sobre un elemento, sin embargo el posicionamiento se toma/establece de forma relativa al offset de su elemento padre

// Eventos del teclado
$('body').keypress(function () { // El método keypress() captura un evento cada vez que un tecla es presionada sobre un elemento que posee el foco
	$('#boxDiv')append($('<div />').addClass('box')); // Código a ejecutar
});

// Foco
$('input').focus(function () { // El evento focus() se captura cuando un elemento posee el foco
	$(this).addClass('inFocus'); // Código a ejecutar
});
$('input').blur(function () { // El evento blur() se captura cuando un elemento pierde el foco
	$(this).removeClass('inFocus'); // Código a ejecutar
});
$('input').on({ // Adicionalmente, el código anterior puede agruparse mediante el manejador on() de esta forma...
	focus: function () {
		$(this).addClass('inFocus')
	},
	blur: function () {
		$(this).removeClass('inFocus')
	}
});

// Submit
$('form').submit(function () { // El evento submit() se aplica a formularios
	// Código a ejecutar una vez enviado el formulario pero antes de procesarlo
});

// Crear y accionar eventos personalizados
$('#sampleID').on({ // Usando manejador on() se pueden declarar eventos personalizados, de la misma forma, se envía como argumento un objeto...
	custom: function () { // ... se declara un método con un nombre personalizado...
		$(this).addClass('green'); // ... y las sentencias a ejecutar
	}
});
$('#sampleId').trigger('custom'); // Aplicando el método trigger() se acciona arbitrariamente cualquier evento posible en un elemento, dicho evento debe enviarse como argumento del método

// Capturar eventos una sola vez
$('#foo').one('click', function () { // El manejador one() captura un evento sólo una vez, en este caso recibe dos argumentos: el evento y la función a ejecutar al realizarse el evento
	alert('Este mensaje se mostrará una sola vez'); // Código a ejecutar
});