//Backjack

// Repartición inicial de dos cartas
var deal = function () { // Esta función reparte una carta
	var card = Math.floor(Math.random() * 52 + 1); // Esta variable es un carta que tiene un valor aleatorio entre 1 y 52
	return card; // La función regresa el valor generado
};
var card1 = deal(); // Esta variable toma el valor de una carta repartida
var card2 = deal(); // Esta variable toma el valor de una carta repartida

// Asignaión real de los valores de la carta
var getValue = function (card) { // Esta función asigna un valor a cada una de las 52 cartas posibles
	if ( (card % 13 === 0) || (card % 13 === 11) || (card % 13 === 12) ) {
		return 10; // Regresa 10
	}
	else if ( card % 13 === 1 ) {
		return 11; // Regresa 11
	}
	else {
		return card % 13; // Regresa el remanente de dividir el número de la carta entre 13
	}
};
/*
Hay 52 cartas
Hay 4 mazos de cartas con 13 cartas cada una: trebol, espada, corazón y coco
El valor de la carta se determina de acuerdo al remanente de dividirlo el número de cartas del mazo: 13
Las cartas correspondientes al joto (11), reina (12) y rey (13) valen 10 cada una, no 11, 12 y 0 respectivamente
El as (1) vale 11 y no 1
De esta manera:
== El primer mazo de cartas de 13:
La carta 1 vale 11
La carta 2 vale 2
La carta 3 vale 3
La carta 4 vale 4
La carta 5 vale 5
La carta 6 vale 6
La carta 7 vale 7
La carta 8 vale 8
La carta 9 vale 9
La carta 10 vale 10
La carta 11 es un joto y vale 10
La carta 12 es una y reina vale 10
La carta 13 es un y rey vale 10
== El segundo mazo de cartas de 13:
La carta 14 vale 11
La carta 15 vale 2
La carta 16 vale 3
La carta 17 vale 4
La carta 18 vale 5
La carta 19 vale 6
La carta 20 vale 7
La carta 21 vale 8
La carta 22 vale 9
La carta 23 vale 10
La carta 24 es un joto y vale 10
La carta 25 es una reina y vale 10
La carta 26 es un rey y vale 10
== Así se procede en los restantes 2 mazos
*/

// Puntuación inicial
var score = function () { // Esta función calcula la puntuación inicial
	return getValue(card1) + (getValuecard2); // regresa la suma de las dos cartas obtenidas una vez procesadas en la función getValue
};