// Factorial con recursión
var factorial = function (n) { // Se define la función para calcular el factorial de n
	if ( n < 0 ) { // Este es el caso terminal que no siempre es necesario pero previene errores inesperados
		return console.log("¡No se permiten números negativos!"); // Para evitar entrar en un bucle infinito si se recibe un número negativo como argumento la función regresa un mensaje
	}
	if ( n === 0 ) { // Éste es el caso base, el momento en que la recursión debe terminar
		return 1; // En el momento que n sea igual a 0 la función regresa 1
	};
	return n * factorial(n - 1); // Este es el caso recursivo de la función llamándose a sí misma; al final regresa el factorial; se debe asegurar que el caso recursivo llevará siempre al caso base
};
factorial(10); // Se llama a la función y se pasa un argumento

// Factorial con while
var loopFactorial = function (n) { // Se define la función para calcular el factorial de n
	result = n; // El resultado es igual a n
	while (n > 1) { // Mientras n sea mayor a 1...
		result = n * (n - 1); // ... el resultado es igual a n multiplcándose por sí misma menos 1 hasta que la condición deje de cumplirse
	};
	return result; // La función regresa al final el resultado
};

// Contando ovejas
var countSheep = function (number) { // Ésta es una función para contar ovejas hasta dormir y recibe un número de ovejas como argumento
	if ( number === 0 ) { // Caso base: en el momento en que el número de ovejas llegue a 0...
		return console.log("Zzzzz"); // ... se está durmiendo
	}
	else { // Caso recursivo: de otra forma...
		console.log("Una oveja menos..."); // ... se informa que hay una oveja menos...
		newNumber = number - 1; // ... una variable llamada newNumber entra en acción y equivale al número actual de ovejas menos 1...
		countSheep(newNumber); // ... y la función se llama a sí misma pasando la variable newNumber como argumento
	};
};

// Recursión vs bucle
var guessNumber = function (number) { // Se define una función para crear un juego sencillo que consiste en adivinar un número, dicho número es recibido como argumento
	guess = prompt("Adivina un número entre 1 y 100:"); // Se declara una variable que almacena la respuesta del usuario
	guess =+ guess; // Con el operador =+ guess se convierte en un número (no confundir con += que sirve para ir sumando valores sobre una variable)
	if ( guess === number ) { // Caso base: si el número del usuario es igual al número a adivinar...
		return console.log("¡Acertaste! El número era " + number); // ... la recursión termina con un return y un mensaje de éxito
	};
	guessNumber(number); // Caso recursivo: la función se llama a sí misma pasando el mismo número como argumento
}; // A diferencia de las recusiones anteriores, en esta ocasión el argumento a pasar sigue siendo el mismo. La ventaja de usar recursión es que puede o no tenerse previsto el número de veces que se ejecutará el ciclo

// Control de flujo
var growBeanstalk = function (years) { // Esta función calcula la altura que tendrá una abichuela mágica de acuerdo al número de años
	if ( years <= 2 ) { // Caso base: si el número de años es igual o menor a 2...
		return 1; // ... regresa un 1
	};
	return growBeanstalk(years - 1) + growBeanstalk(years -2); // Caso recursivo: se usa la secuencia de Fobonacci para calcular el alto de la abichuela
};
var height: growBeanstalk(5); // Se declara una variable para almacenar la altura que tendrá la abichuela después de 5 años
console.log(height); // El resultado de imprime en la consola
/*
La secuencia de Fibonacci consiste consiste en tomar un número e ir sumándo a sus predecesor; siempre comienza con 1 + 1
La secuencia de Fibonacci de 10 es 55, calculándose de la siguiente forma
1    2    3    4    5    6    7    8    9    10
1+   1+   2+   3+   5+   8+   13+  21+  34+  55
La secuencia comienza siempre con 1 + 1 y se va sumando el último número más el anterior inmediato hasta alcanzar el máximo
En este caso práctico el máximo corresponde al argumento years que se envía a la función growBeanstalk
El resultado de la secuencia es 5, calculándose 1 + 1 + 2 + 3 + 5
En control de flujo comienza de izquierda a derecha y de arriba a abajo
Mientras la secuencia recursiva no termine
Cuando se ejecuta un programa, la información se va acumulando hasta su interrupción, en este caso a llegar al caso base y dar por finalizada la recursión
El caso de recusión anterior tiene el siguiente flujo:
growBeanstalk(5) {
	growBeanstalk(4) {
		growBeanstalk(3) {
			growBeanstalk(2) {
				return 1; ======+
			}					+
		+>>>>>>>>>>>>>>>>>>>>>>>+>>>> return 2 =====+
			growBeanstalk(1) {	+					+
				return 1; ======+					+
			}										+
		}											+
	+>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>+>>>> return 3 =====+
		growBeanstalk(2) {							+					+
			return 1; =============== return 1 =====+					+
		}																+
	}																	+
+>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>+>>>> *** return 5 ***
	growBeanstalk(3) {													+
		growBeanstalk(2) {												+
			return 1; ==========+										+
		}						+										+
	+>>>>>>>>>>>>>>>>>>>>>>>>>>>+>>>>>>>>>>>>>>>>>>>>>>>> return 2 =====+
		growBeanstalk(1) {		+
			return 1; ==========+
		}
	}
}
*/

// Aplicando recursión a arrays
var stack = []; // Un array vacío
var countDown = function (n) { // Una función que recibe un entero...
	stack.push(n); // ... y primero la añade al array stack
	if (n === 1) { // En el caso base se verifica si el entero es igual a 1...
		return 1; // ... la función regresa 1
	};
	return countDown(n - 1); // En el caso recursivo se llama nuevamente a la función restando 1 al entero
};
var multiplyEach = function () { // Se define una función que no recibe argumentos
	i = stack.pop(); // Se elimina el último índice de array stack pero se almacena en la variable i. El método pop elimina el último índice de un array y regresando su valor
	x = stack.length; // Posteriormente se determina la longitud actual del array stack, una vez eliminado el último índice
	if ( x === 0 ) { // En el caso base, cuando el array se encuentre vacío...
		return i; // ... se obtiene el valor del último índice sustraído del array stack
	};
	stack[x - 1] = i * stack[x - 1]; // El valor de último índice se actualiza con el resultado de multiplicarse a sí mismo por el valor de i (que corresponde al valor del último índice sustraído del array stack)
	return multiplyEach(); // La función se llama a sí misma nuevamente
};
countDown(10); // Se llama a la función countDown y se envía como argumento un 10; con esto el array stack ahora tiene 10 índices, cada uno con un valor del 10 al 1 respectivamente
console.log(multiplyEach()); // Ahora la función multiplyEach puede trabajar; simplemente se imprime el resultado en la consola

// Modificar valores en un array usando recursión
var capitals = ["berlin", "parIs", "MaDRiD"]; // Se declara un array que tiene 3 índices con los nombres de 3 capitales haciendo mal uso de mayúsculas y minúsculas
var capitalize = function (word) { // Se define una función para corregir el uso de mayúsculas y minúsculas; la función procesa una palabra que recibe como argumento...
	return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase(); // ... y regresa: la primera letra de la palabra (el método charAt regresa la letra con el índice indicado, este caso el índice 0 regresa la primera letra) aplicándole el método toUpperCase para convertirla a mayúscula y se le concatenan los caracateres siguientes (a partir del índice 1) una vez convertidos a minúsculas con el método toLowerCase
};
var fixLetterCase = function (array, i) { // Esta función recibe como argumentos un array y un índice correspondiente para comenzar a operar
	if ( i === array.length ) { // En el caso base, cuando i es igual a la longitud del array (no hay más índices en los qué iterar)
		return; // ... la función termina
	};
	array[i] = capitalize(array[i]); // El valor del índice actual se actualiza una vez procesado en la función capitalize
	fixLetterCase(array, i + 1); // De manera recursiva la función se llama a sí misma para operar en en siguiente índice del array
};
fixLetterCase(capitals, 0); // Se llama a la función recursiva enviando como argumentos el array capitals y el índice 0 para comenzar
console.log(capitals); // Al imprimir el array los valores aparecen ya actualizados

// Modificar valores en arrays multidimensionales
var capitals = [ // Este es un array multidimensional, con 5 índices primarios correspondientes a ciudades de 5 continentes
	["berlin", "parIs", "MaDRiD"], 
	["monTEvideo", "BrASiLia"],
	["dElhi", "toKYo", "BeiJing"],
	["CanBerra"],
	["kiGaLi", "pretoRIA"]
];
var capitalize = function (word) { // Se define una función para corregir el uso de mayúsculas y minúsculas; la función procesa una palabra que recibe como argumento...
	return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase(); // ... y regresa: la primera letra de la palabra (el método charAt regresa la letra con el índice indicado, este caso el índice 0 regresa la primera letra) aplicándole el método toUpperCase para convertirla a mayúscula y se le concatenan los caracateres siguientes (a partir del índice 1) una vez convertidos a minúsculas con el método toLowerCase
};
var fixLetterCase = function (array, x, y) {
	if ( x === array.length ) { // En el caso base, una vez que se han terminado los índices de la primera dimensión la...
		return; // ... función termina
	}
	else if ( y === array[x].length ) { // Cuando los índices de la segunda dimensión del índice actual se han terminado de procesar...
		fixLetterCase(array, x + 1, 0); // ... se llama nuevamente a función aumentando en 1 el valor del índice de la primera dimensión
	}
	else { // En el caso recursivo...
		array[x][y] = capitalize(array[x][y]); // ... se procesa el índice actual...
		fixLetterCase (array, x, y + 1); // ... y se llama recursivamente a la unción aumentando en 1 el valor del índice de la segunda dimensión
	};
};
fixLetterCase(capitals, 0, 0); // Se llama a la función recursiva enviando como argumentos el array capitals y el índice 0 para comenzar en la primera dimensión y 0 también para la segunda dimensión
console.log(capitals); // Al imprimir el array los valores aparecen ya actualizados