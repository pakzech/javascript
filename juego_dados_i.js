// Juego de dados
var die1 = Math.floor(Math.random() * 6 + 1); // Se crea el primer dado
var die2 = Math.floor(Math.random() * 6 + 1); // Se crea el segundo dado
var score; // Se declara una variable para el puntaje
/*
Math.random regresa un número aleatorio entre 0 y 1
luego se multiplica por 6 y se suma 1 para obtener un número entre 1 y 7
Math.floor toma el numero entre () y lo redondea al entero inferior más próximo
Todo esto arroja un número aleatorio entre 1 y 6
El valor del dado será un número entre 1 y 6 cada vez que se ejecute el código
*/
if ( die1 === 1 || die2 === 1 ) { // Si el dado 1 o el dado 2 arrojan un 1...
	score = 0; // ... no se obtiene ningún puntaje
}
else { // De otro modo...
	if ( die1 === die2 ) { // Si ambos dados arrojan el mismo número...
		score = (die1 + die2) * 2; // ... se obtiene un puntaje doble
	}
	else { // De lo contrario...
		score = die1 + die2; // ... sólo se obtienen por puntos la suma de los dos dados
	};
};
console.log("Has tirado un " + die1 + " y un " + die2 + " para un puntaje de " + score); // Al fina la consola nos arroja el valor del dado 1 y del dado 2 y el puntaje obtenido acorde a las reglas