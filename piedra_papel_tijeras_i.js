// Piedra, papel o tijeras
var choices = ["piedra", "papel", "tijeras"]; // Se declara un array con las tres opciones disponibles, éstas se usarán sólo para la elección de la computadora
var computer = choices[Math.floor(Math.random() * 3)]; // Se declara una variable para almacenar la elección de la computadora que corresponderá a un aleatorio entre 0, 1 y 2, luego toma el valor de dicho índice en el array choices
var player = prompt("¿Piedra, papel o tijeras?"); // Se le pregunta al usuario su elección y se almacena en una variable
if ( player !== null ) { // Si la respuesta del usuario no es nula...
	player = player.toLowerCase(); // ... el programa se cerciora de que la respuesta del usuario se guarde siempre en minúsculas
}
var win = "Tu " + player + " derrota a " + computer + ". Tú ganas."; // Se declara una variable para la victoria...
var lose = "Tu " + player + " pierde ante " + computer + ". Tú pierdes."; // ... una para la derrota...
var draw = "Empate: " + player + " con " + computer + "."; // ... y una para el empate
var result = ""; // Se declara una variable para almacenar el resultado final
if ( player === null ) { // Si no se recibe respuesta del jugador...
	result = "¡Adiós!"; // ... el juego termina
}
else if ( player === computer ) { // Si la elección del jugador y la computadora son iguales...
	result = draw; // ... el resultado es un empate
}
else if ( player === "piedra") { // Si el jugador escoge piedra...
	result = ( computer === "tijeras") ? win : lose; // ... usando un operador ternario se determina si la computadora escogió tijeras y el resultado es una victoria del jugador, de otra manera es una derrota
}
else if ( player === "papel") { // Si el jugador escoge papel...
	result = ( computer === "piedra") ? win : lose; // ... usando un operador ternario se determina si la computadora escogió piedra y el resultado es una victoria del jugador, de otra manera es una derrota
}
else if ( player === "tijeras") { // Si el jugador escoge tijeras...
	result = ( computer === "papel") ? win : lose; // ... usando un operador ternario se determina si la computadora escogió papel y el resultado es una victoria del jugador, de otra manera es una derrota
}
else { // Si la respuesta del jugador no es piedra, papel o tijeras
	result = "¡Dije piedra, papel o tijeras!"; // Se notifica al jugador
};
console.log(result); // Al final se imprime el resultado