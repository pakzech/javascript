// Agenda de contactos
var bob = { // Un objeto llamado bob que representa un contacto
    firstName: "Bob", // Propiedad de nombre
    lastName: "Jones", // Propiedad de apellido
    phoneNumber: "(650) 777 - 7777", // Propiedad de teléfono
    email: "bob.jones@example.com" // Propiedad de correo electrónico
};
var mary = { // Un nuevo contacto
    firstName: "Mary",
    lastName: "Johnson",
    phoneNumber: "(650) 888 - 8888",
    email: "mary.johnson@example.com"
};
var contacts = [bob, mary]; // Un array que agrupa los contactos
var totalContacts = contacts.length; // Una variable que equivale al número de índices del array, es decir, al número de contactos en la agenda
var printPerson = function (person) { // Una función que recibe un parámetro llamado person (el parámetro que debe pasarse es el array con uno de sus índices)
	console.log(person.firstName + " " + person.lastName) // La función imprime el nombre y el apellido del contacto
};
var list = function () { // Una función
	for ( i = 0; i < totalContacts; i++ ) { // Un bucle que se repite una vez por cada índice del array
		printPerson(contacts[i]); // Y llama a la función que imprime el nombre y el apellido de contacto
	}
};
list(); // Se llama a la función que enlista todos los contactos de la agenda
var search = function (lastName) { // Esta función busca entre los contactos de la agenda, concretamente en el apellido
    for ( i = 0; i < allContacts; i++ ) { // Ejecuta las búsqueda en cada entrada de la agenda
        if ( lastName === contacts[i].lastName ) { // Si el parámetro recibido es encontrado en el apellido de alguno de los contactos...
            printPerson(contacts[i]); // ... entonces lo imprime
        }
    }
};
search("Jones"); // Se llama a la función de búsqueda con el string Jones
var add = function (firstName, lastName, telephone, email) { // Esta función añade un nuevo contacto y recibe como parámetros el nombre, apellido, teléfono y dirección de correo electrónico
	var newContact = { // Se crear un nuevo objeto
		firstName: firstName, // La propiedad firstName será igual al parámetro recibido para ello
	    lastName: lastName, // La propiedad lastName será igual al parámetro recibido para ello
	    phoneNumber: telephone, // La propiedad firstName será igual al parámetro recibido para ello
	    email: email // La propiedad firstName será igual al parámetro recibido para ello
	};
	contacts[allContacts] = newContact; // Se añade un nuevo ínidice el array y se almacena allí el nuevo contacto.
	/*
	El array contacts actualmente cuenta con 2 índices
	El número del último array es 1, ya que los arrays comienzan en índices 0
	De momento el último índice es contacts[1]
	allContacts almacena el número de índices del array por lo que en este momento equivale a 2
	Al definir contacts[allContacts], en realidad se esta definiendo contacts[2]
	contacts[allContacts] siempre añadirá, por lo tanto, el número de índice siguiente
	*/
};
add(prompt("Nombre:"), prompt("Apellido:"), prompt("Teléfono:"), prompt("Correo Electrónico:")); // Se llama a la función para añadir un nuevo contacto, los parámetros enviados se tomarán de los valores introducidos en el prompt