// Crear un string con notación literal
var str1 = "Hola"; // Se crea una variable y se le asigna un string como valor

// Crear un string con un constructor
var str2 = new String ("Hello"); // Se crea un objeto de la clase nativa String

// Determinar el índice de un substring
var str = "Soy un string cool"; // Se declara una variable con un string
str.indexOf("cool"); // El método indexOf regresa la posición en el índice del string donde aparace el substring enviado como argumento; si éste no se encuentra, el método regresa un -1

// Reemplazar una cadena
str.replace("cool", "aburrido"); // El método replace reemplaza el primer substring enviado como argumento con el segundo substring

// Entender los strings
var str1 = "Él fue el mejor de todos los tiempos"; // Se declara una variable con un string
String.protorype.futureDickens = function () { // Se extiende el prototipo de la clase incorporada String añadiéndole un nuevo método...
	var currentStr = this.toString(); // ... que almacena en una variable el valor del string con el que se está llamando al método futureDickens; el método toString no es necesario pero se asegura de que el valor de this pase como un string
	return currentStr.replace("fue", "será"); // El método regresa el string ya procesado, reemplazando un substring con otro
	// return this.replae("fue", "será"); // Este return también funcionaría correctamente y en caso de usarlo no sería necesario declarar la variable currentStr
};
str1.futureDickens(); // Se llama al nuevo método desde un string

// La clase Math y sus métodos
var num1 = Math.pow(baseNum, powNum); // El método pow de la clase Math toma el primer número y lo eleva a la potencia del segundo
var nun2 = Math.max(aNum1, aNum2); // El método max de la clase Math toma dos o más números y regresa el que tiene mayor valor
var num3 = Math.random(); // El método random de la clase Math genera un número aleatorio entre 0 y 1