// Calcular probabilidades de 1 y dobles
var containsOne = 0; // Se declara una variable para almacenar el número total de veces que puede obtenerse un 1
var isDouble = 0; // Se declara una variable para almacenar el número total de veces que puede obtenerse dobles
var totalCombos = 0; // Se declara una variable para almacenar el número de combinaciones totales posibles al rolar 2 dados
var probability; // Se declara una variable vacía para almacenar la probabilidad
for ( i = 1; i <= 6; i++ ) { // Este for se ejecuratá 6 veces, que es el número de caras del dado. Debe comenzar en 1 y no en 0 ya que la numeración va de 1 a 6 y no de 0 a 5
	for ( y = 1; y <= 6; y++ ) { // A su vez, cada ciclo ejecutará 6 posibles resultados del segundo dado
		if ( i === 1 || y === 1 ) { // Si el dado uno o el dado dos arrojan un 1...
			containsOne++; // ... el valor de la variable containsOne se incrementa en 1
		}
		else if ( i === y ) { // Si el dado uno y el dado dos arrojan dos números iguales...
			isDouble++; // ... el valor de la variable isDouble se incrementa en 1
		}
		totalCombos++; // Por cada vez que se ejecute el for anidado se actualizará el valor de totalCombos
	};
};
probability = containsOne / totalCombos; // Se calcula la probabilidad de obtener un 1 dividiendo el total de veces que se arroja un 1 entre el número total de combinaciones posibles
console.log("La probabilidad de tirar un 1 es de " + probability + " o de " + containsOne + "/" + totalCombos); // Se imprime el resultado final
probability = isDouble / totalCombos; // Se calcula la probabilidad de obtener dobles dividiendo el total de veces que se arrojan dobles entre el número total de combinaciones posibles
console.log("La probabilidad de tirar un 1 es de " + probability + " o de " + isDouble + "/" + totalCombos); // Se imprime el resultado final

// Calcular probabilidades de cada suma de resultados
var newArray = []; // Se declara un array vacío. Posteriormente se actualizarán los valores de sus índices 2 a 12
for ( i = 2; i <= 12; i++ ) { // Este bucle se ejecuta una vez por cada combinación posible al sumar el resultados de los dos dados; el mínimo a obtener es 2 y el máximo son 12
	newArray[i] = 0; // Se declaran los índices 2 a 12 y se establecen a 0.
};
for ( x = 1; x <= 6; x++ ) { // Este for se ejecuratá 6 veces, que es el número de caras del dado. Debe comenzar en 1 y no en 0 ya que la numeración va de 1 a 6 y no de 0 a 5
	for ( y = 1; y <= 6; y++ ) { // A su vez, cada ciclo ejecutará 6 posibles resultados del segundo dado
		newArray[x + y]++; // Cada índice del array entre 2 y 12 se actualiza con cada incidencia aumentando en 1
	};
};
for ( i = 2; i <= 12; i++ ) {
	console.log("La probabilidad de tirar " + i + " es de " + newArray[i] + "/36"); // Se imprime la probabilidad de obtener la suma de 2 a 12
};

// Juevo de dados versión 2
var newArray = []; // Se declara un array vacío. Posteriormente se actualizarán los valores de sus índices 2 a 12
for ( i = 2; i <= 12; i++ ) { // Este bucle se ejecuta una vez por cada combinación posible al sumar el resultados de los dos dados; el mínimo a obtener es 2 y el máximo son 12
	newArray[i] = 0; // Se declaran los índices 2 a 12 y se establecen a 0.
};
for ( x = 1; x <= 6; x++ ) { // Este for se ejecuratá 6 veces, que es el número de caras del dado. Debe comenzar en 1 y no en 0 ya que la numeración va de 1 a 6 y no de 0 a 5
	for ( y = 1; y <= 6; y++ ) { // A su vez, cada ciclo ejecutará 6 posibles resultados del segundo dado
		newArray[x + y]++; // Cada índice del array entre 2 y 12 se actualiza con cada incidencia aumentando en 1
	};
};
var score = 0; // Se declara una variable para almacenar el puntaje obtenido en una tirada
var totalScore = 0; // Se declara una variable para almacenar el puntaje final
var die1 = Math.floor(Math.random() * 6 + 1); // Se crea el primer dado
var die2 = Math.floor(Math.random() * 6 + 1); // Se crea el segundo dado
if ( die1 + die2 ) { // Si la tirada arroja dobles...
	totalScore = 0; // ... el juego termina y el puntaje final es 0
}
else if ( die1 + die2 === 4 ) { // Si la tirada arroja un 4...
	totalScore = -50; // ... el puntaje final es de -50
}
else if ( die1 + die2 === 10 ) { // Si la tirada arrojja un 10...
	totalScore = -100; // ... el puntaje final es de -100
}
else { // En todos los demás casos...
	score = 10 - (newArray[die1 + die2]) * 10; // El puntaje equivale a 10 menos la probabilidad del resultado obtenido (representado por el índice del array correspondiente) multiplicado por 10
};
totalScore += score; // Al final el puntaje total se atualiza sumando el puntaje obtenido
console.log("Has tirado un " + die1 + " y un " + die2 + " para un total de " (die1 + die2)); // Se informa el resultado de tirar los dos dados
console.log("Tu puntaje en esta jugada es de " + score + ". Tu puntaje total es de " + totalScore); // Se informa el puntaje obtenido en la tirada y el total en caso de jugar varias veces