// Agregar nuevos índices a un array
var myArray = ["val1", "val2", "val3"]; // Se declara el array
myArray[myArray.length] = "nuevo valor"; // Se crea un nuevo índice equivalente al número de la longitud del array y se le asigna un nuevo valor
myArray.push("otro valor"); // Se usa el método nativo push segido del valor entre ()

// Eliminar índices de un array
myArray.splice(1, 2); // El método splice sirve para eliminar índices y recibe dos parámetros, el primero corresponde al índice donde se comenzará y el segundo al número de índices a eliminar a partir de ese punto

// Copiar arrays
var a = ["a", "b", "c", "d"]; // Se declara el array a
var b = a; // Se crea el array b que es igual a; bajo este método, cuando se actualiza a también se actualiza b
var c = a.slice(); // Se crea el array c; usando el método slice (no confundir con splice) se crea una copia exacta de array a pero independiente de él
var d = a.slice(1, 2); // Se crea el array d; usando el método slice se crea una copia de array a pero conteniendo sólo dos índices comenzado desde el índice 1

// Arrays multidimensionales
var cards = [ // Se crea un array de dos dimensiones
	["A", "corazones"], // Este índice es un array por sí mismo
	[3, "tréboles"] // Este índice es un array por sí mismo
];
var lastCard = cards[1][1]; // Se crea una nueva variable que almacena el el último índice del último índice del array cards

// Bucles anidados en arrays multidimensionales
var hands = []; // Se declara el array hands; será un array de 3 dimensiones
hands[0] = [ [3,"H"], ["A","S"], [1,"D"], ["J","H"], ["Q","D"] ]; // El índice 1 de hands contiene un array de 2 dimensiones
hands[1] = [ [9,"C"], [6,"C"], ["K","H"], [3,"C"], ["K","H"] ]; // El índice 2 de hands contiene un array de 2 dimensiones
for ( x = 0; x < hands.length; x++ ) { // El primer nivel del for iterará en la primera dimensión de array
	for ( y = 0; y < hands[x].length; y++ ) { // El segundo nivel de for iterará en la segunda dimensión del array
		for ( z = 0; z < hands[x][y].length; z++ ) { // El tercer nivel del for itererá en la tercera dimensión del array
			console.log(hands[x][y][z]); // Se imprime cada valor
		};
	};
};

// Arrays asociativos
var roll = ["robert", "joe", "sharon"]; // Se declara el array roll con una lista de estudiantes
var students = {"sharon" : true, "robert" : true}; // Se crea un objeto (array asociativo) que almacena a los estudiantes presentes en forma de propiedades con valor true
var numPresent = 0; // El número de estudiantes presentes se inicializa en 0
for ( i = 0; i < roll.length; i++ ) { // Este for itera en cada índice de roll, es decir, una vez por cada estudiante en la lista
	if (students[roll[i]]) { // Si el valor actual está presente (con valor true) en forma de propiedad en el objecto (array asociativo)...
		numPresent++; // ... la cuenta de alumnos presentes incrementa en 1
	};
};

// Arrays de arrays asociativos
var hand = [ // Se crea un array indexado que a su vez contiene arrays asociativos
	{"suit":"clubs", "rank":8},
    {"suit":"spades", "rank":"A"},
    {"suit":"hearts", "rank":2},
    {"suit":"hearts", "rank":"k"},
    {"suit":"clubs", "rank":9}
];

// Copiar objectos
var card1 = {"suit":"clubs", "rank": 8}; // Un objeto con dos propiedades
var clone = function (original) { // Se define una función de clonado
	var copy = {} // Se declara un objeto vacío
	for (var key in original) { // Se define un for in para iterar en cada propiedad del objeto a clonar
		copy[key] = original[key]; // Se copia propiedad por propiedad del original a la copia
	};
	return copy; // La función regresa la copia
};
var card2 = clone(card1); // Al llamar a la función se crea una copia independiente