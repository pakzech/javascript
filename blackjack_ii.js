//Backjack
function Card (s, n) { // Se crea una nueva clase para construir cartas y recibe como parámetro s (el palo) y n (el número)
	var suit = s; // Se declara una propiedad privada para el palo y toma el valor de s
	var number = n; // Se declara una propiedad privada para el número y toma el valor de n
	this.getSuit = function () { // Se define un método...
		return suit; // ... que regresa el palo al que pertenece la carta
	};
	this.getNumber = function () { // Se define un método...
		return number; // ... que regresa el número de la carta
	};
	this.getValue = function () { // Se define un método que regresa el puntaje de acuerdo a la carta obenida
		if ( n === 11 || n === 12 || n === 13 ) { // Si se obtiene un joto, una reina o un rey...
			return 10; // ... regresa 10 puntos
		}
		else if ( n === 1 ) { // Si se obtiene un as...
			return 11; // ... regresa 11 puntos
		}
		else { // En todos los demás casos...
			return n; // E... regresa el número de puntos correspondiente al número de la carta
		};
	};
};
var deal = function () { // Se crea una función que reparte una carta
	var s = Math.floor(Math.random() * 4 + 1 ); // Se declara una variable que genera un número aleatorio entre 1 y 4 que corresponderá al palo
	var n = Math.floor(Math.random() * 13 + 1); // Se declara una variable que genera un número aleatorio entre 1 y 13 que corresponde al número
	return new Card (s, n); // La función regresa una nueva carta enviando como parámetros el palo y el número
};
function Hand () { // Se crea una nueva clase para crear una mano
	this.card1 = deal(); // Al crear un nuevo objeto de esta clase...
	this.card2 = deal(); // ... automáticamente se obtienen dos cartas al azar
	this.score = function () { // Se define un nuevo método para obtener el puntaje de la mano...
		return this.card1.getValue() + this.card2.getValue(); // ... y regresa (llamando al método getValue) la suma de los valores de ambas cartas
	};
};
var myHand = new Hand (); // Se crear un nuevo onjeto que genera una nueva mano de 2 cartas
var yourHand = new Hand (); // Se crear un nuevo onjeto que genera una nueva mano de 2 cartas
console.log("Yo obtuve un puntaje de " + myHand.score() + "; tú  obtuviste un puntaje de " + yourHand.score()); // Se imprime el puntaje obtenido en ambas manos
if ( myHand.score() > yourHand.score() ) { // Si mi puntaje es mayor que el tuyo...
	console.log("¡Yo gano!"); // ... entonces yo gano
}
else if ( myHand.score() < yourHand.score() ) { // Si tu puntaje es mayor que el mío...
	console.log("¡Tú ganas!"); // ... entonces tú ganas
}
else { // En cualquier otro caso...
	console.log("Hemos empatado"); // ... hemos empatado
};