/***
Funciones y parámetros globales
***/
var quitTil = function (str) {
	str = str.replace(/á/gi, 'a');
	str = str.replace(/é/gi, 'e');
	str = str.replace(/í/gi, 'i');
	str = str.replace(/ó/gi, 'o');
	str = str.replace(/ú/gi, 'u');
	str = str.replace(/ñ/gi, 'x');
	return str;
};
var quitArt = function (str) {
	str = str.replace(/(de |del |y |la |las |los |van |von )/gi,'');
	return str;
};
var quitVoc = function (str) {
	str = str.replace(/(a|e|i|o|u)/gi,'');
	return str;
};
var quitCon = function (str) {
	str = str.replace(/b|c|d|f|g|h|j|k|l|m|n|p|q|r|s|t|v|w|x|y|z/gi,'');
	return str;
};
var malCon = [
	'BUEI','BUEY','CACA','CACO','CAGA','CAGO','CAKA','CAKO','COGE','COJA','KOGE','KOJO','KAKA','KULO',
	'MAME','MAMO','MEAR','MEAS','MEON','MION','COJE','COJI','COJO','COLA','CULO','FETO','GUEY','JOTO',
	'KACA','KACO','KAGA','KAGO','KOLA','MOCO','MULA','PEDA','PEDO','PENE','PITO','PUTA','PUTO','QULO',
	'RATA','ROBO','RUIN'
];
var checkMal = function (str, data) {
	for (i = 0; i < malCon.length; i++) {
		if (str === malCon[i] && data === 'curp') {
			str = str.charAt(0) + 'X' + str.slice(2);
		}
		else if (str === malCon[i] && data === 'rfc') {
			str = str.substring(0, 3) + 'X';
		};
	};
	return str;
};

/***
Cálculo de CURP
***/
var getCURP = function (nombre, aPaterno, aMaterno, d, m, a, sexo, estado) {
	var curp = '';
	var getDig = function (curp) {
		var segRaiz = curp.substring(0,17);
		var chrCaracter = '0123456789ABCDEFGHIJKLMNÑOPQRSTUVWXYZ';
		var intFactor = [17];
		var lngSuma = 0.0;
		var lngDigito = 0.0;
		for (i = 0; i < 17; i++) {
			for (j = 0; j < 37; j++) {
				if (segRaiz.substring(i, i + 1) === chrCaracter.substring(j, j + 1)) {
					intFactor[i] = j;
				};
			};
		};
		for (k = 0; k < 17; k++) {
			lngSuma = lngSuma + ((intFactor[k]) * (18 - k));
		};
		lngDigito = 10 - (lngSuma % 10);
		if (lngDigito === 10) {
			lngDigito = 0;
		};
		return lngDigito;
	};
	nombre = quitTil(quitArt(nombre).replace(/(jose |josé |maría |maria )/gi,''));
	aPaterno = quitTil(quitArt(aPaterno));
	aMaterno = quitTil(quitArt(aMaterno));
	var elem1 = (aPaterno.charAt(0) + quitCon(aPaterno).charAt(0) + aMaterno.charAt(0) + nombre.charAt(0)).toUpperCase();
	elem1 = checkMal(elem1, 'curp');
	var elem2 = a + m + d;
	var elem3 = sexo + estado;
	var elem4 = (quitVoc(aPaterno.slice(1)).charAt(0) + quitVoc(aMaterno.slice(1)).charAt(0) + quitVoc(nombre.slice(1)).charAt(0)).toUpperCase();
	curp = elem1 + elem2 + elem3 + elem4 + '0';
	var digVer = getDig(curp);
	return curp + digVer;
};

/***
Cálculo de CURP
***/
var getRFC = function (nombre, aPaterno, aMaterno, d, m, a) {
	var rfc = '';
	nombre = quitTil(quitArt(nombre));
	aPaterno = quitTil(quitArt(aPaterno));
	aMaterno = quitTil(quitArt(aMaterno));
	var elem1 = (aPaterno.charAt(0) + quitCon(aPaterno).charAt(0) + aMaterno.charAt(0) + nombre.charAt(0)).toUpperCase();
	elem1 = checkMal(elem1, 'rfc');
	var elem2 = a + m + d;
	rfc = elem1 + elem2;
	return rfc;
};

getCURP('ivan', 'pacheco', 'martínez', '10', '03', '86', 'H', 'MC');
getRFC('ivan', 'pacheco', 'martínez', '10', '03', '86');