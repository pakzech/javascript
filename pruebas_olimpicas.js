// Dar seguimiento a corredores en una pista olímpica
var runner = prompt("¿Cuál es tu nombre?"); // Se requiere el nombre del corredor
if ( runner.length > 0 ) { // Si el campo no se dejó en blanco...
	console.log("Bienvenido a las pruebas olímpicas, " + runner); // ... se le da la bienvenida al corredor
}
else { // De otra manera...
	console.log("Tu nombre no puede estar en blanco."); // ... se le indica que no puede dejar su nombre en blanco
};
var carlos = [9.6,10.6,11.2,10.3,11.5]; // Este es un array que almacena los tiempos registrados por el corredor en sus carreras
var liu = [10.6,11.2,9.4,12.3,10.1]; // Este es un array que almacena los tiempos registrados por el corredor en sus carreras
var timothy = [12.2,11.8,12.5,10.9,11.1]; // Este es un array que almacena los tiempos registrados por el corredor en sus carreras
var calculateAverage = function (raceTimes) { // Esta función calcula el tiempo promedio del corredor y recibe como parámetro el array de registro de tiempos
	var totalTime = 0; // El tiempo inicial de los corredores es 0
	for ( i = 0; i < raceTimes.length; i++ ) { // Este bucle se repite por cada índice en el array, es decir, por cada registro de tiempo del corredor
		totalTime += raceTimes[i]; // Y los suma a su tiempo total
	};
	var averageTime = totalTime / raceTimes.length; // Esta variable tiene como valor la suma total de los tiempos registrador entre el número de índices del array, es decir, entre el número de carreras
	return averageTime; // La función regresa el promedio registrado por el corredor
};
var isQualified = function (runner) { // Esta función determina si el corredor será aceptado en el equipo, de acuerdo a su promedio registrado. Recibe como parámetro al corredor (el array con sus tiempos registrados)
	var averageTime = calculateAverage(runner); // El promedio registrado por el corredor se obtiene al procesarlo en la función calculateAverage
	if ( averageTime >= 11.5 ) { // Si su promedio es mayor o igual a 11.5...
		console.log("Cerca, pero no has pasado el corte"); // ... el corredor no está calificado para entrar al equipo
	}
	else { // De otra manera...
		console.log(); // ... se le notifica que está dentro del equipo
	};
};
isQualified(liu); // Se llama a la función y se pasa como parámetro a uno de los corredores
isQualified(timothy); // Se llama a la función y se pasa como parámetro a uno de los corredores