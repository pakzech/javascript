// Caja registradora con cambio
var CashRegister = { // Se crea el objeto para la caja registradora
	total: 0, // Se declara la propiedad que almacenará el total de la compra y se inicializa a 0
	change: 0, // Se declara la propiedad que almacenará el monto del cambio a devolver y se inicializa a 0
	currency: [5.00, 1.00, 0.50, 0.10], // Se crea un array que almacena los valores de las monedas que hay en caja
	coin: ["monedas de $5", "monedas de $1", "monedas de ¢50", "monedas de ¢10"], // Se crea un array que almacena los nombres de las monedas que hay en caja
	count: 0, // Se declara una propiedad que más tarde almacenará la cuenta de monedas para dar el cambio
	setTotal: function (amount) { // Se define un método que recibe como argumento el valor total de la compra...
		this.total = amount; // ... y actualiza el valor de la propiedad global total con el valor del argumento recibido
	},
	getPaid: function (amountPaid) { // Se define un método que recibe como argumento el monto pagado
		if ( this.total > amountPaid ) { // Si el monto recibido en pago en menor al monto de la compra...
			console.log("No es suficiente"); // ... se indica que no es suficiente
		}
		else { // De otra manera...
			this.change = (amountPaid - this.total).toFixed(2); // ... se actualiza el valor de la propiedad que almacena el monto de cambio restando el total al monto pagado y se trunca redondeándolo a 2 decimales
			console.log("Dar de cambio:"); // Se indica que hay que dar cambio...
			this.makeChange(0); // ... y se llama al método makeChange enviando un 0 como parámetro
		};
	},
	makeChange: function (index) { // Este método procesa el cambio y da como resultado el número y tipo de monedas para dar el cambio; se basa en recursión en listas y recibe un argumento que equivale al índice del array currency con el que se comenzará la recursión
		if ( index >= this.currency.length ) { // Si el índice recibido es mayor o igual a la longitud de array currency...
			return; // ... termina la recursión
		}
		else { // De otra manera...
			if (this.change >= this.currency[index]) { // ... si el monto actual de cambio es mayor o igual al valor de la moneda actual...
				this.change -= this.currency[index]; // ... se resta ese valor al monto del cambio...
				this.count++; // ... se aumenta en 1 la cuenta de la moneda actual...
				this.makeChange(index); // ... y de manera recursiva se llama nuevamente al mismo método pasando como argumento el mismo índice
			}
			else { // Una vez que el monto del cambio es menor que el de la moneda actual...
				console.log(this.count + " " + this.coin[index]); // ... se imprime la cuenta final de monedas y su nombre...
				this.count = 0; // ... se reinicia la cuenta de monedas...
				this.makeChange(index + 1); // ... y se llama de manera recursiva al mismo método y se suma 1 al índice que se envía como parámetro
			};
		};
	}
};
CashRegister.setTotal(83.70); // Se envía el valor de la compra
CashRegister.getPaid(100); // Se envía el monto pagado