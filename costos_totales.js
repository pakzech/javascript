// Costos totales de una empresa
var calculateTotalCosts = function (salary, numWorkers, city) { // Se define una función para calcular los costos totales recibiendo el salario, el número de empleados y la ciudad de la sucursal
    var fixedCosts = 5000; // Los costos fijos son de 5000
    var variableCosts = salary * numWorkers; // Los costos variables dependen del salario y del número de trabajadores
    var rent; // La renta depende de la ciudad
    switch (city) { // Se crea un switch que recibe el valor de la ciudad y contempla 3 casos
        case "NYC": // Si la ciudad es Nueva York...
           rent = 30000; // ... la renta es de 30000
        break;
        case "BEJ": // Si la ciudad es Beijing...
            rent = 25000; // ... la renta es de 25000
        break;
        default: // En todas las demás ciudades...
            rent = 10000; // ... la renta es de 10000
        break;
    }
    return fixedCosts + variableCosts + rent; // Al final se obtienen los costos totales sumando los costos fijos, los costos variables y el valor de la renta
};
calculateTotalCosts(40000, 3, "BEJ");
calculateTotalCosts(40000, 3, "NYC");
calculateTotalCosts(40000, 3, "MEX");