// Acceso a propiedades de un objeto
var james = { // Un objeto
	job: "programmer", // Una propiedad
	married: false // Una propiedad
};
var aProperty = "job"; // Una variable que tiene como valor el nombre de una propiedad (en string)
console.log(james.job); // Se accede a la propiedad job mediante notación de puntos
console.log(james["job"]); // Se accede a la propiedad job mediante notación de corchetes
console.log(james[aProperty]); // Se accede a la propiedad job mediante notación de corchetes haciendo referencia a una variable que tiene como valor el nombre (en string) de una propiedad

// Determinar un tipo de valor
var anObj = { name: "Soy un objeto" };
var aString = "Soy un string";
var aNumber = 42;
console.log(typeof anObj); // El método typeof determina qué tipo de valor es anObj, en este caso, objeto
console.log(typeof aString); // El método typeof determina qué tipo de valor es aString, en este caso, string
console.log(typeof aNumber); // El método typeof determina qué tipo de valor es aNumber, en este caso, número

// Determinar si un objeto posee cierta propiedad
var anObj = { name: "Soy un objeto" }; // Un objeto con propiedad name
console.log(anObj.hasOwnProperty("name")); // El método hasOwnProperty aplicado a un objeto, indicando entre () el nombre de la propiedad, determina si dicha propiedad está presente en el objeto y regresa true o false
console.log(anObj.hasOwnProperty("nickname")); // En este caso el método hasOwnProperty regresa false
var suitcase = { // Un objeto
	shirt: "Hawaiian" // Con una propiedad llamada shirt
};
if ( anObj.hasOwnProperty("shorts") === true ) { // Si el objeto tiene un propiedad llamada shorts...
	console.log(anObj.shorts); // ... se imprime el valor de la propiedad
}
else { // De otra manera...
	anObj.shorts = "Blue"; // Se define la propiedad y se le asigna un valor...
	console.log(anObj.shorts); // ... y luego se imprime
};

// Bucle for in
var nyc = { // Un objeto
	fullName: "New York City", // Una propiedad
    mayor: "Michael Bloomberg", // Una propiedad
    population: 8000000, // Una propiedad
    boroughs: 5 // Una propiedad
};
for (var x in nyc) {
	console.log(x);
}
/*
Este es un bucle for
var x está declarando una variable local que se asigna a cada una de las propiedades del objeto
in nyc indica el objeto sobre el que se va a trabajar
El bucle se ejecuta una vez por cada propiedad que contenga el objeto
El código a ejecutar se encuentra entre {}
En este caso la sentencia a ejecutar es imprimir por separado cada propiedad del objeto
*/
for (var x in nyc) {
	console.log(nyc[x]);
}
/*
Este bucle actúa de la misma forma que el anterior
Esta vez imprime los valores de cada propiedad
En este tipo de bucle sólo se permite usar la notación de corchetes
En este caso al hacer referncia a x en la notación de corchetes no deben usarse "" ya que x representa en sí mismo un string
*/

// Clases en JavaScript
function Dog (breed) {
	this.breed = breed;
};
/*
Este es un construnctor de objetos
En realidad es la definición de una clase y es el prototipo de los objetos que deriven de él
El prototipo determina qué propiedades y métodos tendrán sus objetos derivados y qué se podrá hacer con ellos y qué no
*/

// Propiedades y métodos propios de un objeto
var buddy = new Dog ("Golder Retriever"); // Este es un nuevo objeto, en realidad una nueva instancia de la de la clase Dog. Heredado del prototipo sólo puede tener una propiedad llamada breed
buddy.bark = function () { // Se está declarando un nuevo método llamado bark para el objeto buddy
	console.log("Woof");
};
var snoopy = new Dog ("Beagle"); // Este es un nuevo objeto, en realidad una nueva instancia de la clase Dog. Heredado del prototipo sólo puede tener una propiedad llamada breed
buddy.bark(); // Al llamar al método bark del objeto buddy, éste funciona correctamente ya que fue declarado expresamente como un método propio del objeto
snoopy.bark(); // Al llamar al método bark del objeto snoopy, éste no responde ya que de acuerdo al prototipo no existe, tampoco fue declarado como un método propio del objeto

// Extender el prototipo de una clase
function Dog (breed) { // Se define una nueva clase
	this.breed = breed; // Se define una nueva propiedad para la clase
};
Dog.prototype.bark = function () { //El método prototype aplicado a la clase, seguido de un nuevo método o propiedad usando notación de puntos, actualiza la clase
	console.log("Woof"); // De esta manera se está agregando un núevo método al prototipo de la clase
};

// Herencia de clases
function Animal (name, numLegs) { // Ésta es una clase llama Animal y recibe dos parámetros...
	this.name = name; // ... cuyos valores serán asignados a una propiedad
	this.numLegs = numLegs;
};
Animal.prototype.sayName = function () { // Se está extendiendo la clase Animal añadiéndole un nuevo método llamado sayName
	console.log("Hola, mi nombre es " + this.name);
};
function Penguin (name) { // Se está definiendo una nueva clase llamada Penguin que sólo recibe el parámetro name
	this.name = name;
	this.numLegs = 2; // La propiedad numLegs se asigna manualmente a 2 ya que todos los pingüinos tienen 2 patas
};
Penguin.prototype = new Animal (); // Se está extendiendo la clase Penguin convirtiéndola en una nueva instancia de la clase Animal
var penguin = new Penguin ("Pingu"); // Éste es un nuevo objeto de la clase Penguin
penguin.sayName(); // A pesar de que la clase Penguin no tiene definido el método sayName, lo ha heredado de la clase Animal, por lo que al llamarlo responde correctamente
function Emperor (name) { // Se está definiendo una nueva clase llamada Emperor
	this.name = name;
};
Emperor.prototype = new Penguin (); // Emperor a su vez es una nueva instacia de la clase Penguin
var emperor = new Emperor ("Pingüino Emperador"); // Éste es un nuevo objeto de la clase Emperor
console.log(emperor.numLegs); // La herencia funciona tanto en métodos como en propiedades, por lo que la consola arrojará un 2 heredado de la propiedad numLegs de la clase Penguin
/*
A este modelo se le llama cadena de prototipos
Cuando de llama un método o una propiedad de determinado objeto, se busca primero entre los propios métodos y propiedades del mismo objeto
Si se encuentra el método o propiedad, la cadena de prototipos sube un nivel en la herencia
Hace eso de manera sucesiva hasta encontarlo o no
*/

// Verificar si un objeto es una instancia de una clase
console.log(emperor instanceof Animal); // instanceof sirve para verificar si un objeto es una instancia de una clase, en este caso se verifica si emperor es una instancia por herencia de la clase Animal; regresa un booleano

// Propiedades públicas y privadas
function Person (first, last, age) { // Una nueva clase con 3 parámetros
	this.firstName = first; // Todas las propiedades son por defecto públicas, lo que significa que pueden ser accesibles libremente desde fuera de la propia clase
	this.lastName = last; // Una propiedad pública
	this.age = age; // Una propiedad pública
	var bankBalance = 7500; // Ésta es una propiedad privada y no puede accederse desde fuera de la clase. Se define con var y no con this
};
var john = new Person ("John", "Smith", 30); // Un nuevo objeto de la clase Person
var myFirst = john.firstName; // El objeto puede acceder libremente a la propiedad firstName de la clase
var myLast = john.lastName; // El objeto puede acceder libremente a la propiedad lastName de la clase
var myAge = john.age; // El objeto puede acceder libremente a la propiedad age de la clase
console.log(john.bankBalance); // No se puede acceder directamente a la propiedad bankBalance en el objeto john ya que ésta es una propiedad privada

// Métodos públicos y privados
function Person (first, last, age) { // Una nueva clase con 3 parámetros
	this.firstName = first; // Todas las propiedades son por defecto públicas, lo que significa que pueden ser accesibles libremente desde fuera de la propia clase
	this.lastName = last; // Una propiedad pública
	this.age = age; // Una propiedad pública
	var bankBalance = 7500; // Ésta es una propiedad privada y no puede accederse desde fuera de la clase. Se define con var y no con this
	this.getBalance = function () { // Ésta es un método público...
		return bankBalance; // ... y regresa el valor de una propiedad privada
	};
	var returnBalance = function () {
		return bankBalance;
	};
};
var john = new Person ("John", "Smith", 30); // Un nuevo objeto de la clase Person
var myBalance = john.getBalance(); // Esta variable tiene un valor asigando que resulta de llamar al método público getBalance. Para obtener un valor a partir de un método, primero debe asignarse a una variable
console.log(myBalance); // La variable se imprime correctamente regresando el valor de la propiedad privada bankBalance, ya que ésta fue accedida desde un método público
var returnBalance = john.returnBalance(); // // Esta variable tiene un valor asigando que resulta de llamar al método privado returnBalance. Para obtener un valor a partir de un método, primero debe asignarse a una variable
console.log(returnBalance); // La variable no imprime el valor de la propiedad privada bankBalance, ya que ésta fue accedida desde un método privado

// Argumentos en métodos
function Person (first, last, age) { // Una nueva clase
	this.firstName = name; // Una propiedad pública
	this.lastName = last; // Una propiedad pública
	this.age = age; // Una propiedad pública
	var bankBalance = 7500; // Una propiedad privada
	this.askTeller = function (pass) { // Un método público que recibe un argumento llamado pass
		if (pass === 1234) { // Si el argumento recibido es igual a 1234...
			return returnBalance; // ... entonces el método regresa el valor de bankBalance
		}
		else { // De otra manera...
			return "Contraseña incorrecta."; // ... se indica que la contraseña es incorrecta
		};
	};
};
var john = new Person ("John", "Smith", 30); // Se crear un nuevo objeto de la clase Person
john.askTeller(1234); // Se llama al método público askTeller enviando 1234 como argumento pass

// Llamando funciones externas desde un objeto
var setName = function (yourName) { // Esta es una función que recibe como parámetro u nombre...
	this.name = "Tu nombre es " + yourName; // ... y lo asigna como valor a la propiedad name del objeto que lo haya llamado
};
var human = {
	setHumanName: setName // Este método asigna un nombre al objeto usando la función externa setName; en este caso setName se escribe sin ()
};
human.setHumanName("Ivan"); // Se llama al método setHumanName enviándole en parámetro que pasará automáticamente a la función externa setName
console.log(human.name); // Ahora el objeto human tiene una propiedad name con un valor y puede imprimirse
