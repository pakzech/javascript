// Operador or
var yourName = "Ivan"; // Se declara una variable para el nombre
var gender = "male"; // Se declara una variable para el sexo
if ( gender === "male" || gender === "female" ) { // La condicionante if contiene ||, un or, por lo que el if se ejecuta si una u otra condición se cumple
	result = true;
}
else {
	result = false;
};

// Operador and
var yourName = "Ivan";
var gender = "male";
var result;
if ( yourName.length > 0 && gender.length > 0 ) { // La condicionante if contiene &&, un and, por lo que el if se ejecuta si todas las condiciones se cumplen
	result = "Gracias";
}
else {
	result = "Por favor asegúrate que tanto tu nombre como tu sexo estén completos";
};

// Condicionantes anidados
var yourName = "Ivan";
var gender = "male";
var result;
if ( yourName.length > 0 && gender.length > 0 ) { // Primero se verifica que se cumplan dos condiciones; si ambas se cumplen se puede hacer otra verificación...
	if ( gender === "male" || gender === "female" ) { // ... un if anidado que verifca nuevas condiciones
		result = "Gracias";
	}
	else {
		result = "Por favor indica male o female para sexo";
	};
}
else {
	result = "Verifica ambos: nombre y sexo"; // Esta es la respuesta por omisión en caso que la primera condición no se cumpla
};
var topThree = true;
var winner = false;
var result;
if ( topThree === false ) {
	result = "Lo sentimos, te has ido con las manos vacías";
}
else {
	if ( winner === true ) { // Una consicional anidada puede situarse tanto en el if original como en el else
		"Felicidades, has ganado la medalla de oro";
	}
	else {
		"Nada mal, has obtenido una medalla";
	};
};

// ¿Tienes edad para conducir?
var canIDrive = function (myAge, legalDrivingAge) { // Se define una función que recibirá como parámetros la edad del usuario y la edad legal para conducir
	if ( myAge >= legalDrivingAge ) { // Si la edad del usuario es igual o mayor a la edad legal para conducir...
		return true; // Se obtiene un true
	}
	else {
		return false; // En todo caso se obtiene un false
	};
};
var myAge = prompt("¿Cuál es tu edad?"); // Se requiere interacción del usuario para asignar un valor a su edad
var legalDrivingAge = 18; // La edad legal para conducir se estable manualmente en 18
if ( canIDrive(myAge, legalDrivingAge) ) { // Si la función canIDrive evalúa como true (no es necesario especificar === true)...
	console.log("Felicidades. Ya tienes edad para conducir") // Se obtiene la confirmación de que se tiene edad legal para conducir
}
else {
	console.log("Lo sentimos. Aún no tienes edad para conducir"); // En todo caso se informa que no se posee edad legal para conducir
};

// Sentencia switch
var jacketColor = "black"; // Se declara la varibale y se le asigna un valor
var result; // Se declara la variable para el resultado
switch (jacketColor) { // La sentencia switch se usa para manejar varias posibilidades; se coloca como parámetro la variable que se va a analizar
	case "black": // Se define el caso (case) y la posible incidencia seguido de :
		result = "Paga $300"; // Y la acción a tomar si éste valida como true; en este caso se asigna un valor a la variable result
	break; // Con break se cierra el caso
	case "brown": // Un segundo caso
		result = "Paga $200";
	break;
	case "green": // Un tercer caso
		result = "Paga $100";
	break;
	default: // Se define un caso predeterminado si ninguno de los anteriores se cumple
		result = "No se tiene en existencia ese color";
	break;
};

// Un switch para confirmar nacionalidad
var born = prompt("¿Cuál es tu país de nacimiento?");
var result;
switch (born) {
	case "México":
		result = "Nacido en México";
	break;
	default:
		result = "Nacido en el extranjero";
	break;
};

// Operadores ternarios
var x = 10; 
var y = 15;
if ( x > y ) { // Este es un if esle común
	result = "¡Buen trabajo!";
}
else {
	result = 20;
};
result = x > y ? "¡Buen trabajo" : 20; // Este es un operador ternario realiza la misma acción que el if else anterior pero en una sola línea; en esre caso primero se coloca la variable afectada seguida de = y la condición a evaluar; luego se coloca ? seguido de los posibles valores separados por :

// La sintaxis de un operador ternario es 'condición ? resultado1 : resultado 2;'
var myAge = prompt("¿Cuál es tu edad?");
answer = myAge >= 18 ? "adulto" : "menor"; // Si myAge es mayor o igual a 18, entonces answer = adulto; de lo contrario answer = menor