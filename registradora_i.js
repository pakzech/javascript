// Caja registradora
function StaffMember (name, discountPercent) { // Una clase para aplicar a los empleados de la tienda un descuento. Recibe como parámetros el nombre y el descuento aplicable
	this.name = name; // La propiedad name tiene el valor del argumento name recibido
	this.discountPercent = discountPercent; // La propiedad discountPercent tiene el valor del argumento discountPercent recibido
};
var ivan = new StaffMember ("Ivan", 20); // Un nuevo objeto de la clase StaffMember que corresponde un empleado con su respectivo porcentaje de descuento
var cashRegister = { // Este objeto crea una caja registradora
	total: 0, // El total de la compra está inicializado a 0
	lastTransactionAmount: 0, // Esta propiedad rastrea la última transacción y está inicializada a 0
	add: function (itemCost) { // Este es el método para añadir un artículo
		this.total += itemCost; // El total de la compra se actualiza sumando el costo del artículo a comprar
		lastTransactionAmount = itemCost;
	},
	scan: function (item, quantity) { // Este metodo escanea los artículos a comprar y recibe como argumentos el nombre del artículo y la cantidad
		switch (item) { // Este switch recibe el artículo...
			case "eggs": this.add( 0.98 * quantity ); break; // ... y usa el método add para actualizar el total de la compra
			case "milk": this.add( 1.23 * quantity ); break; // Cada caso tiene registrado el nombre del artículo y el costo por unidad...
			case "magazine": this.add( 4.99 * quantity ); break; // ... y lo multiplica por la cantidad...
			case "chocolate": this.add( 0.45 * quantity ); break; // ... al final envía el resultado como argumento itemCost al método add
		};
	},
	voidLastTransaction: function () { // Este método cancela la última operación
		this.total -= lastTransactionAmount; // Resta la última transacción al total de la compra registrada
	},
	applyStaffDiscount: function (employee) { // Este método aplica el descuento apropiado a cada empleado
		var dicount = ( employee.discountPercent / 100 ) * this.total; // Crear una nueva variable para calcular el descuento llamando a la propiedad discountPercent del objeto recibido como argumento y dividiéndola entre 100 para después multiplicarla por el total de la compra
		this.total -= dicount; // Al final actualiza el total de la compra restándole el descuento correspondiente
	}
};
cashRegister.scan('eggs',1); // Se escanea 1 huevo
cashRegister.scan('milk',1); // Se escanea 1 leche
cashRegister.scan('magazine',3); // Se escanean 3 revistas
cashRegister.applyStaffDiscount(ivan); // Se llama al método para aplicar un descuento enviando como argumento un objeto de la clase StaffMember
console.log('El monto de la compra es de $'+cashRegister.total.toFixed(2)); // Al final se imprime el valor final de la propiedad total limitándolo a 2 decimales