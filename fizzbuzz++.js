// FizzBuzz++
var FizzBuzzPlus = { // Se crea el objeto para FizzBuzz++
	isFizzBuzzie: function (n) { // Se define un método para determinar si un número es múltiplo de 3 o de 5 pero no de ambos
		if ( ( n % 3 === 0 ) && ( n % 5 === 0 ) ) { // Si el número recibido como argumento es divisible entre 3 y 5...
			return false; // ... entonces se obtiene un false
		}
		else if ( ( n % 3 === 0 ) || ( n % 5 === 0 ) ) { // Pero si el número es divisible entre 3 o 5...
			return true; // ... entonces se obtiene un true
		}
		else { // En todos los demás casos...
			return false; // ... se obtiene un false
		};
	},
	getFizzBuzzSum: function (max) { // Se define un método para obtener la suma de todos los números que son divisibles entre 3 o 5
		var sum = 0; // Se declara la variable para almacenar el valor de la suma i se inicializa en 0
		for ( i = 1; i < max; i++ ) { // Se crea un bucle que se repetirá desde el 1 hasta un número antes del máximo recibido como argumento
			if ( this.isFizzBuzzie(i) === true ) { // Usando el método isFizzBuzzie se determina si el valor actual de i es divisible entre 3 o 5, y en ese caso...
				sum += i; // ... el valor de i se suma a la variable sum
			};
		};
		return sum; // El método regresa la suma final
	},
	getFizzBuzzCount: function (max) { // Se define un método para obtener el número de incidencias de todos los números que son divisibles entre 3 o 5
		var count = 0; // Se declara la variable para almacenar el valor de la incidencia: el número de veces que se obtiene un número múltiplo de 3 o 5, y se inicializa en 0
		for ( i = 1; i < max; i++ ) { // Se crea un bucle que se repetirá desde el 1 hasta un número antes del máximo recibido como argumento
			if ( this.isFizzBuzzie(i) === true ) { // Usando el método isFizzBuzzie se determina si el valor actual de i es divisible entre 3 o 5, y en ese caso...
				count++; // ... el valor de i se incrementa en 1
			};
		};
		return count; // El método regresa la cuenta final
	},
	getFizzBuzzAverage: function (max) { // Se define el método para obtener el promedio de todos los número que son divisibles entre 3 o 5
		return this.getFizzBuzzSum(max) / this.getFizzBuzzCount(max); // Usando los métodos anteriores se divide la suma de dichos números entre el número de veces que aparecen y regresa el resultado
	}
};
console.log(FizzBuzzPlus.getFizzBuzzAverage(100)); // Se imprime en la consola el resultado del método para obtener el promedio de todos los números divisibles entre 3 o 5 inferiores a 100