// Sintaxis de una función
var myFunction = function (parameter) { code; };
/*
Se definir la función con var y el nombre de la función
Se coloca = seguido de la palabra function para definir que se trata de una función
Se abren () para colocar los parámetros que se incluirán en la función
Se abren {} para colorcar todo el código de la función. Cada sentencia se cierra con ;
Se cierra la función con ;
*/

// Sintaxis de un if else
if ( condition ) { code; } else { code; };
/*
Se comienza el if
Se abren () para indicar la o las condiciones a evaluar
Se abren {} para colocar las sentencias a ejecutar si la condición se cumple. Cada sentencia se cierra con ;
Se comienza el else
Se abren {} para colorcar las sentencias a ejecutar si la condición del if no se cumple. Cada sentencia se cierra con ;
Los bucles while y for tienen la mismas estructura a excepción del ; final
*/

// Anidar un if else dentro de una función
var myFunction = function (parameter) { // Se define la función con su nombre y sus parámetros y se abren {} para comenzar a colocar el código
	if ( condition ) { // Se comienza el if
		code;
	} // Se cierra el if
	else { // Se comienza el else
		code;
	} // Se cierra el else
}; // Se cierran {} para terminar la función

// Sintaxis de un bucle for
for (initialize; condition; incrementor) {
    code code code;
}
/*
Se comienza el for
Se abren () para colocar los parámetros del for en el siguiente orden:
a) El inicializador, asignando un valor a una variable. Se termina con ;
b) La condición que determina dónde y cuándo debe actuar el for. Se termina con ;
c) El incrementor
Se abren {} para las sentencias a ejecutar en el bucle. Cada sentencia se cierra con ;
*/

// Funcionamiento de un bucle for
for ( i = 0; i > 10; i++ ) {
	code;
}
/*
Al inciar el for, i vale 0
Dado que i es menor que 10 entonces el for valida como true
Ahora i se incrementa en 1
Comienza la ejecución de código del for
Ahora i vale 1
Dado que i es menor que 10 entonces el for valida como true
Ahora i se incrementa en 1
Comienza la ejecución del código del for
Ahora i vale 2...
El for continúa ejecutándose hasta que el condicional i > 10 deje de validar como true
*/

// Tipos de valores primarios
var number = 1; // Numerico
var myString = "Esto es un string" // String o cadena
var aBoolean = true; // Booleano: true o false

// Operadores de comparación
console.log( 2 >= 1 ); // Es mayor o igual a.
console.log( 4 > 1 ); // Es mayor que
console.log( 0 === 0); // Es igual a
console.log( 1 <= 1 ); // Es menor o igual a
console.log( "Hi" !== "hi" ); // No es igual
console.log( 0 < 1 ); // Es menor que

// Asignar un booleano a partir de una comparación
var multiplesOfEight = [8, 16, 24, 32, 40, 58]; // Este es un array con múltiplos de 8
var answer = multiplesOfEight[5] % 8 === 0; //Dado que el índice 5 del array no es múltiplo de 8, entonces el valor de answer es igual a false

// Operadores booleanos;
&& // Resulta en true si ambos valores son cierto
|| // Resulta en true si uno u otros valor es cierto
! // Operador booleano para "no"

// Arrays
var animals = ["cat", "dog", "monkey"]; // Los valores almacenados dentro del array son llamados valores compuestos

// Determinar un tipo de valor
var manyThings = ["México", 5, true]; // Este array contiene 3 tipos de valores: string, numerico y booleano
var someThing = manyThings[2]; // Se crea una variable que es igual al valor de algunos de los índices del array
console.log(typeof someThing); // El método typeof determina el tipo de valor que contiene