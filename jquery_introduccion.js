// Seleccionar elementos en jQuery
$('#myElement'); // En jQuery se seleccionan los elementos del DOM con $('elemento')
$('#id'); // Seleccionat un elemento por ID
$('.class'); // Seleccionar elementos por clase
$('document'); // Seleccionar el documento

// Ejecutar una o varias sentencias cuando el documento carga
$('document').ready( // En jQuery se selecciona el documento y se aplica el método ready; todo lo que se encuentre entre los () se ejecutará una vez listo el documento
	function () { // Esta es una función anónima...
		alert('¡El documento está listo!'); // ... que manda una alerta
	};
);

// Capturar el evento click de un elemento
var callback = function () { // Se define una función llamada callback...
	alert('Esto es una llamada'); // ... que crea una alerta
};
$('document').ready( // Al cargar el documento...
	function () { // ... se define una función anónima
		$('myElement').click(callback); // Se captura el evento click en el elemento con ID myElement en el momento en que se produzca, el cual llama a la función callback
	};
);

// Objetos de jQuery
var thing = $(); // Al asignar una variable a un elemento de jQuery se está creando un objeto
alert(typeof thing); // Al aplicar el operador typeof se sabe que la variable thing es un objeto

// Función nativa proxy
var o = { // Un objeto...
	'x': 6 // ... que tiene una propiedad x con valor de 6
};
var x = 9; // Una variable global x con valor de 9
var alertX = function () { // Una función que...
	alert(this.x); // ... envía una alerta con el valor de la variable global x; this, al encontrarse fuera de un objeto hace referencia a la ventana por lo que this.x hace referencia  la variable global de x
};
var $proxiedAlertX = $.proxy(alertX, o); // La función nativa proxy de jQuery toma dos argumentos: una función y un objeto de tal manera que ahora la función alertX está ligada al objeto o por lo que this.x ya no hará referencia a la variable global x sino a la propiedad x del objeto o

// Es o no es número
var a = isNaN(verify); // El método nativo isNaN verifica si el valor recibido es un número legible y regresa true o false

// Uso de valores
var b = $('#input').val(); // El método val obtiene el valor de un elemento
$('#input').val('Nuevo valor'); // Se puede asignar un valor a un elemento mediante el método val y colocando el valor entre los ()

// Anchura de un elemento
var c = $('#element').width(); // El método width obtiene el valor en píxeles de la anchura de un elemento
$('#element').width(newWidth); // Se puede asignar una nueva anchura colocando el nuevo valor entre los ()

// Método each
$d = $('.elements'); // Se crea un nuevo objeto de jQuery, el cual es un array que contiene todos los elementos de la case elements
d.each(aFunction); // Se aplica el método each al objeto d tomando como argumento una función que iterará en cada uno de los elementos del array, es decir, en cada elemento con la clase elements