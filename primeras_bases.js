// Declarar una variable
var primerVariable = "Esta es una variable"; // Las cadenas de texto (strings) se encierran entre ""
var segundaVariable = 42; // Los números (enteros) no se encierran entre ""

// Cada sentencia debe terminarse con ;

// Crear alerta y confirmación
alert("Hola Mundo"); // Esto es una alerta de JavaScript
confirm("Confirma que quieres seguir adelante"); // Mensaje de confirmación con botones de OK y Cancelar

// Operaciones con números
2 + 2; // Suma
2 - 1; // Resta
2 * 2; // Multiplicación
9 / 3; // División
9 % 2; // Remanente de una División

// Obtener el número de caracteres
primerVariable.length;
"Hello".length;

// Substrings
primerVariable.substring(0,2); // El método substring obtiene, en este caso, los primeros 2 caracteres
var nuevoSubstring = primerVariable.substring(0,2); // Almacena un substring en una variable
var otroSubstring = "Hello".substring(0,3); // El método substring obtiene, en este caso, los primeros 3 caracteres

// Reemplazar strings
"cadena reemplazada".replace("reemplazada", "reemplazada por esto"); // El primer string dentro del método replace es el texto a reemplazar, el segundo es el texto con el que se reemplazará

// Mayúsculas y minúsculas 
"esto se convertirá a mayúsculas".toUpperCase(); // Método para convertir a mayúsculas
"ESTO SE CONVERTIRÁ A MINÚSCULAS".toLowerCase(); // Método para convertir a minúsculas

// Arrays
var primerArray = [1, 4, 6]; // Un array es una estructura de datos que permite almacenar múltiple valores encerrados entre [] y separados por ,
var unaVariable = primerArray[2]; // Esta variable tiene asignado el tercer valor del array. El número que se encuentra entre [] es el índice del array que comienza a contarse desde el 0
var days = [0, 1, 2]; // Nuevo array
days[0] = "Monday"; // Se ha asignado un valor al índice 0 del array
days[1] = "Tuesday"; // Se ha asignado un valor al índice 1 del array
days[2] = "Wednesday"; // Se ha asignado un valor al índice 2 del array

// Sentencia de control if
var number = 7; // El signo de = asigna un valor
if ( number === 7 ) { // La sentencia de control if es una condicional; === hace una comparación de valores
	console.log("¡Tu número es 7!"); // Si la declaración es verdadera entonces se ejecuta, en este caso, la sentencia console.log
};

// Sentencia de control else
var nombre = "Ivan";
if ( nombre === "Ivan" ) { // Si tu nombre es Ivan...
	console.log("Tu nombre es Ivan"); // La consola confirma que tu nombre es Ivan. Esta sentencia también debe cerrarse con ;
}
else { // La sentencia de control else podría interpretarse como un "de otro modo". Si tu nombre no es Ivan...
	console.log("Tu nombre no es Ivan"); // La consola confirma que tu nombre no es Ivan. Esta sentencia también debe cerrarse con ;
};

// if y else con prompt
var nombre = prompt("¿Cuál es tu nombre"); // El método prompt requiere al usuario ingresar un valor
if ( nombre === "Ivan" ) {
	console.log("Tu nombre es Ivan");
}
else {
	console.log("Tu nombre no es Ivan");
};

// Sentencia de control else if
var number = prompt("Adivina qué número entre 1 y 10 estoy pensando"); // Este es un pequeño juego de adivinanza de números
if ( number === "7" ) { // Si el número tecleado es 7...
	console.log("¡Acertaste!"); // Se ejecuta esta sentencia
}
else if ( number == "6" ) { // La sentencia de control else if podría interpretarse como "pero sí". Pero si el número tecleado es 6...
	console.log("Te has quedado un poco corto") // Se ejecuta esta sentencia
}
else { // Si el número tecleado no es 7 (que cumple la condicional if) ni 6 (que cumple la condicional else if)...
	console.log("Lo siento, no has acertado") // Se ejecuta esta sentencia
};

// Comparación de valores
var number = prompt("Selecciona un número entre 1 y 10");
if ( number === 5 ) { // === igual a *** !== no es igual a
	console.log("Tu número es igual a 5");
}
else if ( number > 5 ) { // > menor que
	console.log("Tu número es mayor a 5");
}
else if ( number < 5 ) { // mayor que
	console.log("Tu número es menor a 5");
};

// Incremental
var i = 0; // Se establece el valor de i en 0
i++; // Se incrementa una unidad a i
i++; // Se incrementa otra unidad a i
console.log("i es igual a " + i); // Ahora i es igual a 2; el símbolo + sirve para concatenar valores

// Decremental
var i = 2; // Se establece el valor de i en 2
i--; // Se decrementa una unidad a i
i--; // Se decrementa otra unidad a i
console.log("Ahora i es igual a" + i); // Ahora i es igual a 0

// Bucle for
var i; // Se declara la variable
for ( i = 0; i < 2; i++ ) { // inicialización; condición; incremento; el iterador i debe ser declarado
	console.log("Ahora i es igual a " + i); // Mientras la condición de que i sea menor a 2 sea true se ejecuta esta sentencia
} // El bucle for no debe cerrarse con ;

// Bucle while
var i = 0; // Se declara la variable y se le asigna un valor
while ( i < 2 ) { // Mientras la condición evalúe como true...
	console.log ("Ahora i es igual a" + i);
	i++; // ... entoces añadir una unidad a i
} // El bucle while no debe cerrase con ;
var times = 0;
while (times > 0 && times < 3) { // Para ejecutar este while, hay dos condiciones y ambas (&&) deben cumplirse, sin embargo dada su naturaleza no pueden ser ambas ciertas a un mismo tiempo
	console.log("el bucle se ha ejecutado"); // Por lo tanto el bucle es omitido
	times++; // Sentencia a ejecutar
}

// Bucle do/while
var i = 0;
do { // El bucle do/while ejecuta por lo menos una vez determinada acción independientemente de que su condición sea true o false
	console.log("Esta es la iteración " + (i + 1) + ".");
	i++; // Sentencia a ejecutar
}
while ( i !== 4 ) // Condición