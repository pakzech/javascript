// Ejecutar un for con incremental de 1
var i; // Se declara la variable i
for ( i = 1; i <= 7; i++ ) { // El bucle se inicializa en 1 y se ejecuta hasta que i sea menor o igual a 7 con un incremento de 1 por cada ciclo
	console.log(i);
};

// Ejecutar un for con un incremental mayor a 1
var i; // Se declara la variable i
for ( i = 2; i <= 7; i += 2 ) { // El bucle se inicializa en 2 y se ejecuta hasta que i sea menor o igual a 7 con un incremento de 2 por cada ciclo
	console.log(i);
};
/*
El incremental no guarda en la memoria el valor de i
Por ello no se declara como i + 2 sino que al valor actual se suman 2 con +=
*/

// Ejecutar un for con decremental de 1
var i; // Se declara la variable i
for ( i = 5; i >= 0; i-- ) { // El bucle se inicializa en 5 y se ejecuta hasta que i sea mayor o igual a 0 con un decremento de 1 por cada ciclo
	console.log(i);
};

// Bucles en arrays
var i; // Se declara la variable i
var animals = ["cat", "dog", "ferret"]; // Un array con 3 índices
for ( i = 0; i < animals.length; i++ ) { // El bucle se inicializa en 0 (el número de primer índice en cualquier array) y se ejecuta hasta que i sea menor al número de índices con un incremento de 2 por cada ciclo
	console.log(animals[i]); // Este bucle imprime el valor de cada índice del array
};
var length = animals.length; // Declarar un variable que almacene el número de índices del array es más rápido, 
for ( i = 0; i < animals.length; i++ ) { // Ya el bucle leerá una sóla vez el valor de la variable lenght antes de ejecurase y no una vez por cada ejecución como en el caso de array.length, será más rápido y efectivo
	console.log(animals[i]);
};

// Bucles en strings
var word = "code"; // Una variable con valor de string
for ( i = 0; i < word.length; i++ ) { // El bucle se inicializa en 0 y se ejecuta hasta que i sea menor al número de caracteres del string contenido en la variable word
	console.log(word[i]); // Al igual que los arrays, los strings contienen índices equivalentes a cada caracter. Este bucle imprime cada caracter del string
};

// Bucles en substrings
var substring = function (input, start, end, i) { // Esta función recibe un string, el número de caracter donde debe empezar y terminar
	for ( i = start; i <= end; i++ ) { // Este bucle se repite una vez por cada caracter comenzando con el argumento start y terminando con el argumento end
		console.log(input[i]); // Imprime cada caracter del substring
	}
};
substring("lorem ipsum dolor", 6, 10); // Se llama a la función enviando un string, el caracter con el que comienza y el caracter con el que termina el substring
var substring = function (input, start, end, i) { // Esta función recibe un string, el número de caracter donde debe empezar y terminar
	var subset = ""; // Se declara una variable para almacenar el substring
	for ( i = start; i <= end; i++ ) { // Este bucle se repite una vez por cada caracter comenzando con el argumento start y terminando con el argumento end...
		subset += input[i]; // ... y actualiza el valor de subset aumentando cada caracter del substring
	}
	return subset;
};
console.log(substring("lorem ipsum dolor", 6, 10)); // AL final puede imprimirse en una sola línea el substring seleccionado

// Bucle while
var condition = false; // Esta variable tiene como valor un false
while (!condition) { // El bucle se ejecutará hasta que la condición deje de evaluar como true. Aunque en este caso siempre es true...
	console.log("Este bucle debe correr sólo una vez");
	break; // ... por lo que se coloca un break al final para ejecutarlo sólo una vez
}

// Ejecutar un while con incremental de 1
var i = 1; // Se declara la variable i con valor de 1
while ( i <= 5 ) { // Mientras i sea igual o menor a 5...
	console.log(); // ... imprime en la consola el valor de i...
	i++; // ... incrementa i en 1 y el bucle comienza un nuevo ciclo hasta que la condición regrese un false
}

// Ejecutar un while con incremental de 2
var i = 1; // Se declara la variable i con valor de 1
while ( i <= 5 ) { // Mientras i sea igual o menor a 5...
	console.log(); // ... imprime en la consola el valor de i...
	i += 2; // ... incrementa i en 2 y el bucle comienza un nuevo ciclo hasta que la condición regrese un false
}

// Cortar un nombre con while
var name = "Ivan Pacheco"; // Esta variable contiene un string con un nombre y un apellido
var getFirstName = function (fullName) { // Esta función regresará sólo el nombre al recibir el nombre completo como argumento
	var firstName = ""; // Se declara la variable para el nombre; de momento es una cadena vacía
	var i = 0; // Se declara una variable i inicializada en 0
	var length = fullName.length; // Se declara una variable para almacenar la longitud de caracteres del string a procesar
	var next = fullName[0]; // Se declara una variable para trabajar en el siguiente caracter al que se está procesando en el bucle while y se inicializa en 0
	while ( i < length && next !== " " ) { // Este while se ejecutará mientras haya caracteres disponibles (en todo caso, hasta el final de la cadena) siempre y cuando el siguiente caracter a analizar no sea igual a un espacio " "
		firstName += fullName[i]; // EL valor de firstName se actualizará añadiendo el caracter correspondiente...
		i++; // ... luego i se incrementará en 1...
		next = fullName[i]; // ... y el valor del siguiente caracter se actualizará con el valor i
	}
	return firstName; // Al final la función devuelve el primer nombre hasta donde encontró un espacio en el string recibido originalmente
};
console.log(getFirstName(name)); // Al final, cuando se llama la función enviando como argumento la variable name, se obtiene sólo el nombre

// Bucle recursivo con incremental de 1
var countUp = function (current) { // Esta es una función que recibe un número como argumento
	if ( current < 5 ) { // Ésto se llama caso recursivo. Si el valor del argumento es menor a 5...
		console.log(current); // ... se imprime su valor...
		countUp(current + 1); // ... y se llama de nuevo a sí mismo aumentando 1 al valor del argumento
	};
	if ( current === 5 ) { // Ésto se llama caso base. Cuando el valor del argumento sea igual a 5...
		console.log(current); // ... se imprime su valor pero aquí termina el bucle ya que no se vuelve a hacer la llamada
	};
};
countUp(1); // Se llama a la función y se envía un 1 como parámetro

// Bucle recursivo con decremental de 1
var countDown = function (current) { // Esta es una función que recibe un número como argumento
	if ( current > 1 ) { // Ésto se llama caso recursivo. Si el valor del argumento es mayor a 1...
		console.log(current); // ... se imprime su valor...
		countDown(current - 1); // ... y se llama de nuevo a sí mismo restando 1 al valor del argumento
	};
	if ( current === 1 ) { // Ésto se llama caso base. Cuando el valor del argumento sea igual a 1...
		console.log(current); // ... se imprime su valor pero aquí termina el bucle ya que no se vuelve a hacer la llamada
	};
};
countUp(5); // Se llama a la función y se envía un 5 como parámetro

// for anidados
var i; // Una variable para usar en el bucle
var j; // Una variable para usar en el bucle
for ( i = 1; i <= 3; i++ ) { // Este bucle principal se ejecutará tres veces
	for ( j = 1; j <= 5; j++ ) { // ESte bucle se ejecutará cinco veces...
		console.log(j); // ... e imprimirá los números del 1 al 5
	}
}; // AL final se imprimen los número del 1 al 5, tres veces

// Imprimir tablas (arrays dentro de arrays)
var table = [ // Se declara una tabla
	["Persona",		"Edad",		"Ciudad"], // El primer índice del array es otro array
	["Sue",			22,			"San Francisco"], // El segundo índice del array es otro array
	["Joe",			45,			"Halifax"]  // El tercer índice del array es otro array
];
var rows = table.length; // Se declara una variable para obtener el número de filas de la tabla
var r; // Esta variable servirá para operar operar como un índice en la tabla, es decir, como una fila
for ( r = 0; r < rows; r++ ) { // Este for se ejecutará una vez por cada fila en la tabla, en este caso serán 3 veces
	var cells = table[r].length; // Se declara una variable para obtener el número de celdas de cada fila en la tabla, en este caso son 3 por cada fila
	var c; // Esta variable servirá para operar como un indice en cada fila de la tabla, es decir, como una celda dentro de una fila
	var rowText = ""; // Esta es una variable con un string vacío que posteriormente almacenará el valor de cada celda
	for ( c = 0; c < cells; c++ ) { // Este for se ejecutará una vez por cada celda en la fila actual, en este caso 3 veces
		rowText += table[r][c]; // El for agrega a la variable rowText el valor de la celda actual
		if ( c < cells -1 ) {
			rowText += "  "; // Este condicional agrega dos espacios en blanco después de cada valor de cada celda
		};
		console.log(rowText); // Al final se imprime el valor de la variable rowText que arroja todos los valores de cada fila y celda en la tabla
	};
}; // La operación completa se ejecutará una vez por cada fila dentro de la tabla, pero antes de comenzar de nuevo el bucle completo, un fon anidado operará una vez por cada celda dentro de la fila