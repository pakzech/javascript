// Crear un objeto mediante notación literal
var Me = { // El obeto se crear de la misma manera que las variables a excepción que después de = de abren {} y también se cierran con;
    age: 26, // Esta es una propiedad del objeto. Cada propiedad se define con el nombre seguido de : y el valor
    country: "Mexico" // Cada propiedad se cierra con , a excepción de la última
};

// Acceder a las propiedades de un objeto mediante notación de punto
var age1 = Me.age; // Se declara una variable cuyo valor equivale a la propiedad de un objeto
var country1 = Me.country; // En este caso se accede mediante Objeto.propiedad

// Acceder a las propiedades de un objeto mediante notación de corchetes
var age1 = Me["age"]; // Se declara una variable cuyo valor equivale a la propiedad de un objeto
var country1 = Me["country"]; // En este caso se accede mediante Objeto["propiedad"]

// Crear un objeto mediante un constructor
var bob = new Object(); // Se declara el objecto de la misma forma que una variable; después de = se coloca new Object() y se cierra con ;
bob.name = "Bob Smith"; // Las propiedades se declaran por separado con una notación de punto seguido de = y el valor a asignar
bog.age = 24;

// Métodos en objetos
var bob = new Object(); // Se define el objeto
bob.name = "Bob Smith"; // Una propiedad
bob.age = 30; // Una propiedad
bob.setAge = function (newAge) { // Este es un método, que actúa de la misma forma que una función
	bob.age = newAge; // En este caso, el método asigna un nuevo valor a la propiedad age
};
bob.setAge(20); // Se llama al objeto seguido del método. La sintaxis en Objeto.nombreDelMétodo

// Método para calcular el año de nacimiento
var bob = new Object(); // Se define el objeto
bob.name = "Bob Smith"; // Una propiedad
bob.age = 30; // Una prpopiedad
bob.setAge = function (newAge) { // Un método...
	bob.age = newAge; // ... que actualiza el valor de un parámetro
};
bob.getYearOfBirth = function () { // Un método...
	return 2012 - bob.age; // ... que resta al año actual la edad y regresa el año de nacimiento
};
console.log(bob.getYearOfBirth()); // Esto imprime el resultado del método bob.getYearOfBirth

// Métodos globales para usarse con más de un objeto
var setAge = function (newAge) { // Este método es una función ordinaria. Debe definirse antes que los objetos que van a asociarse con él
	this.age = newAge; // Actualiza la propiedad age del objeto que se haya usado para llamar al método. this actúa como un marcador de posición que será sustituido por el nombre del objeto al momento de llamarlo
};
var bob = new Object();
bob.age = 30;
bob.setAge = setAge; // En esta línea se asocia al objeto con el método y se le asigna como valor, en este caso, el nombre del mismo método

// Métodos con no globales con this
var rectangle = new Object();
rectangle.length = 3;
rectangle.width = 4;
rectangle.setLength = function (newLength) { // Éste es un método que sólo puede usarse con el objeto rectangle ya se que se ha antepuesto su nombre antes del nombre del método
	this.length = newLength; // En este caso this sólo se usa para indicar el objecto actual
};
rectangle.setWidth = function (newWidth) { // Éste es un método que sólo puede usarse con el objeto rectangle ya se que se ha antepuesto su nombre antes del nombre del método
	this.width = newWidth; // En este caso this sólo se usa para indicar el objecto actual
};

// Cálculos dentro de un metodo
var square = new Object(); // Se define el objeto
square.sideLength = 6; // Una propiedad
square.calcPerimeter = function () { // Un método...
	return this.sideLength * 4; // ... que regresa el resultado de multiplicar el valor de la propiedad sideLength * 4 del objeto actual
};
square.calcArea = function () { // Un método
	return this.sideLength * this.sideLength; // .. que regresa el resultado de multiplicar el valor de la propiedad sideLength por sí misma del objeto actual
};
var p = square.calcPerimeter(); // Esta variable toma el resultado del método calcPerimeter aplicado al objeto square
var a = square.calcArea(); // Esta variable toma el resultado del método calcArea aplicado al objeto square

// Objectos en constructor con propiedades
function Person (name, age) { // Este es un costructor de objectos llamados Person y recibe dos parámetros
	this.name = name; // La propiedad name del objeto será igual al valor del parámetro name
	this.age = age; // La propiedad age del objeto será igual al valor del parámetro age
	species = "Homo Sapiens"; // No todas las propiedades deben ser definidas entre () y como en este caso puede hacer propiedades estáticas
};
var george = new Person("George Washington", 275); // Este constructor crea un objeto Person con propiedades name y age ya definidas

// Constructores con métodos
function Rectangle (length, width) { // Este es un constructor de objetos Person
	this.length = length; // Una propiedad
	this.width = width; // Una propiedad
	this.calcPerimeter = function () { // Este es un método y actúa como una función local
		return (this.length * 2) + (this.width * 2); // ... que regresa el perímetros del objecto
	};
	this.calcArea = function () { // Este es un método y actúa como una función local
		return this.length * this.width; // ... que regresa el perímetros del objecto
	};
};
var rex = new Rectangle(7, 3); //Crea un objeto llamado Rectangle y envía los parámetros que luego se asignarán a las propiedades length y width
var p = rex.calcPerimeter(); // Esta variable toma el valor que regrese el método cuando es llamado por el objeto indicado
var a = rex.calcArea(); // Esta variable toma el valor que regrese el método cuando es llamado por el objeto indicado

// Arrays de objetos
function Person (name, age) { // Un constructor de objetos, en este caso personas que tienen nombre y edad como parámetros
	this.name = name;
	this.age = age;
};
var family = new Array(); // Se declara una nuevo arreglo vacío llamado family
family[0] = new Person("alice", 40); // Se declara un índice del array y se le asigna como valor un nuevo objeto con sus respectivos parámetros de nombre y edad
family[2] = new Person("bob", 42); // Un nuevo índice del array que equivale a un nuevo objecto
family[3] = new Person("michelle", 8); // Un nuevo índice del array que equivale a un nuevo objecto
family[4] = new Person("timmy", 6); // Un nuevo índice del array que equivale a un nuevo objecto

// Combinar objetos con otros herramientas
function Person (name, age) { // Un constructor de objetos, en este caso personas que tienen nombre y edad como parámetros
	this.name = name;
	this.age = age;
};
var family = new Array(); // Se declara una nuevo arreglo vacío llamado family
family[0] = new Person("alice", 40); // Se declara un índice del array y se le asigna como valor un nuevo objeto con sus respectivos parámetros de nombre y edad
family[2] = new Person("bob", 42); // Un nuevo índice del array que equivale a un nuevo objecto
family[3] = new Person("michelle", 8); // Un nuevo índice del array que equivale a un nuevo objecto
family[4] = new Person("timmy", 6); // Un nuevo índice del array que equivale a un nuevo objecto
for (i = 0; i <= family.length; i++) { // Este bucle se ejecuta una vez por cada índice del array family
	console.log(family[i].name); // .... e imprime la propiedad name de cada objecto de cada índice
}
var billy = new Object("Billy", 30); // Un nuevo objeto Person
var alice = new Object("Alice", 25); // Un nuevo objeto Person
var ageDifference = function (person1, person2) { // Esta es una función que toma dos parámetros que deben ser objetos...
	return person1.age - person2.age; // ... de los que extrae la propiedad age y devuelve la diferencia entre uno y otro
};
var diff = ageDifference(billy, alice); // Esta variable toma su valaro después de pasar dos objetos Person como parámetros de la función ageDifference