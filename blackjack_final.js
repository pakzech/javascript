// Constructor de cartas
function Card (s, n) { // Se crea una clase para crear cartas; recibe como parámetros s (el palo) y n (el número)
	var suit = s; // Se declara una propiedad privada para el palo y se le asigna el valor de s
	var number = n; // Se declara una propiedad privada para el número y se le asigna el valor de n
	this.getSuit = function () { // Se define un método público...
		return suit; // ... que regresa el palo de la carta
	};
	this.getNumber = function () { // Se define un método público...
		return number; // ... que regresa el número de la carta
	};
	this.getValue = function () { // Se define un método para obtener el valor de la carta
		if (number === 1) { // Si la carta es un As...
			return 11; // ... se obtienen 11 puntos
		}
		else if (number > 10) { // Si la carta es un joto, reina o rey...
			return 10; // ... se obtienen 10 puntos
		}
		else { // Todas las demás cartas...
			return number; // ... valen lo que indica su número
		};
	};
};
var deal = function () { // Se define una función que reparte una carta
	var suit = Math.floor(Math.random() * 4 + 1); // El palo de la carta es un número aleatorio entre 1 y 4
	var number = Math.floor(Math.random() * 13 + 1); // El número de la carta es un número aleatorio entre 1 y 13
	return new Card (suit, number); // La función regresa un nuevo objeto de la Clase Card con los parámetros generados; en resumen, la función regresa una nueva carta
};
function Hand () { // Se crea una clase para la mano
	var cards = [deal(), deal()]; // Se declara una propiedad privada en forma de array
	this.getCards = function () { // Se define un método público...
		return this.cards; // ... que regresa las cartas en la mano
	};
	this.score = function () { // Se define un método para obtener el puntaje de la mano
		var sum = 0; // Se declara una propiedad privada para almacenar la suma de puntos de la mano
		var aces = 0; // Se declara una propiedad privada para almacenar el número de Ases que contiene la mano
		for (i = 0; i < cards.length; i++) { // Este bucle itera una vez por cada carta en la mano
			if (cards[i].getValue() === 11) { // Por cada carta cuyo puntaje sea igual a 11, es decir, por cada As en la mano
				aces++; // La cuenta de Ases aumenta en 1
			};
		};
		while (aces > 1){ // Mientras el número de Ases sea mayor a 1...
			sum -= 10; // ... se restan 10 puntos a puntaje de la mano (ya que sólo un As puede valer 11, los restantes valen 1)
			aces--;
		};
		return sum; // El método regresa la suma de puntos totales de la mano
	};
	this.printHand = function () { // Se define un método para imprimir la mano
		var actualCard = ""; // Se declara una variable para almacenar la carta actual sobre la que se iterará
		for (i = 0; i < cards.length; i++) { // Este bucle itera una vez por cada carta en la mano
			actualCard = cards[i]; // El valor de actualCard se actualiza con el de la carta sobre la que se está iterando
			console.log("Carta " + actualCard.getNumber() + " del palo " + actualCard.getSuit()); // Se imprime el número y el palo de la carta
		};
	};
	this.hitMe = function () { // Se define un método para tomar una nueva carta y añadirla a la mano
		cards.push(deal()); // Se agrega un nuevo índice al array cards con una nueva carta
	};
};
var playAsDealer = function () { // Se define una función para jugar como repartidor
	var dealer = new Hand(); // Se crea una nueva mano
	while (dealer.score() < 17) { // Mientras el puntaje de la mano sea menor a 17...
		dealer.hitMe(); // ... se agrega una nueva carta a la mano
	};
	return dealer; // La función regresa la mano del repartidor
};
var playAsUser = function () { // Se define una función para jugar como usuario
	var user = new Hand(); // Se crea una nueva mano
	user.printHand(); // Se imprime la mano del usuario
	confirm = confirm("¿Tomar una carta más?"); // Una vez conocida la mano del usuario, se le pregunta si desea tomar una carta más y agregarla a la mano
	while (confirm) { // Mientras el usuario confirme que desea tomar una carta más...
		user.hitMe(); // ... se agrega la carta...
		confirm = confirm = confirm("¿Tomar una carta más?"); // ... y se le pregunta nuevamente hasta que responda que no
	};
	return user; // La función regresa la mano del usuario
};
var declareWinner = function (dealerHand, userHand) { // Se define una función para declarar al ganador del juego tomándo como parámetros la mano del repartidor y la del usuario
	result = ""; // Se declara una variable para almacenar el resultado
	if (dealerHand.score() > 21) { // Primero se determina si el puntaje de la mano del repartidor es mayor a 21, en ese caso...
		if (userHand.score() > 21) { // ... si el puntaje de la mano del usuario también es mayor a 21...
			result = "¡Empate!"; // ... el resultado es un empate
		}
		else { // De otra manera...
			result = "¡Tú ganas!"; // ... se declara la victoria del usuario
		};
	}
	else if (dealerHand.score() > userHand.score()) { // Si el puntaje de la mano del repartidor es mayor al del usuario...
		result = "Tú pierdes"; // ... se declara la derrota del usuario
	}
	else if (dealerHand.score() < userHand.score()) { // Si el puntaje de la mano del repartidor es menor al del usuario...
		result = "¡Tú ganas!"; // ... se declara la victoria del usuario
	}
	else { // En los demás casos...
		result = "¡Empate!"; // ... se declara el empate
	};
	return result; // La función regresa el resultado
};
var playGame = function () { // Se define una función para hacer funcionar el juego
	var dealerHand = playAsDealer(); // Se declara la mano del repartidor
	var userHand = playAsUser(); // Se declara la mano del usuario
	console.log("La mano del repartidor es " + dealerHand.printHand() + " para un puntaje de " + dealerHand.score()); // Se imprime la mano y el puntaje del repartidor
	console.log("La mano del usuario es " + userHand.printHand() + " para un puntaje de " + userHand.score()); // Se imprime la mano y el puntaje del repartidor
	console.log(declareWinner(dealerHand, userHand)); // Al final se imprime el resultado
};
playGame(); // Se llama a la función para jugar